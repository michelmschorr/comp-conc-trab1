/*
Wave Function Collapse.
Recebe o endereço de um arquivo de entrada, contendo os modulos possiveis, as regras e o tamanho desejado da grid.
*/
void wfc(char* adress){

    
    
    __lowest_entropy=__num_modules;
    __num_cells_lowest_entropy = 0;



    __affected_cells = createQueue();
    __lowest_entropy_cells = (struct node**)malloc(sizeof(struct node*));
    __lowest_entropy_cells[0] = NULL;


    //Cria e preenche a grid
    Cell_2d** grid;
    //grid = setup(adress);
    grid = setup_test(); //teste base
    //grid = setup("/home/michelms/root/local/education/higher/ufrj/ciencia_computacao/semesters/2022.1/Computacao_Concorrente/trabalho/trab/seq/src/setup.json");
    //posicao randomica na lista de celulas de menor entropia
    int rand_cell=0;

    //o node retirado da lista de celulas de menor entropia
    struct node* next_node;

    //proxima posicao a ser collapsada
    Pos_2d next;
    next.lin = choose_rand(__size_lin-1);
    next.col = choose_rand(__size_col-1);

    //primeira inserção na lista
    insertLast_non_repete(__lowest_entropy_cells, next);
    __num_cells_lowest_entropy=1;


    //roda enquanto houverem celulas nao colapsadas
    while(!isEmpty(__lowest_entropy_cells)){
        //deletando a proxima celula a ser collapsada da lista
        delete(__lowest_entropy_cells, rand_cell);
        __num_cells_lowest_entropy--;


        //collapsando celula
        printf("Colapsando (%d,%d)\n", next.lin+1, next.col+1);
        collapse(grid, next);
        printf("ncle: %d\n", __num_cells_lowest_entropy);


        //se a lista de celulas de menor entropia esta vazia, faz uma varredura da grid por celulas nao collapsadas
        if(__num_cells_lowest_entropy == 0){
            __lowest_entropy = __num_modules;
            backtrack(grid);
        }


        //proxima celula a ser collapsada
        rand_cell = choose_rand(__num_cells_lowest_entropy-1);

       
        //node da proxima celula a ser collapsada
        next_node = find(__lowest_entropy_cells, rand_cell); 
        if(next_node==NULL){
            break;
        }


        //atualizando next
        next = next_node->data;


        print_grid(grid);
    }
    print_grid(grid);
    printf("Fim!\n");
}
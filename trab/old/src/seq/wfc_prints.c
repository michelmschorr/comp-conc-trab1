#include "../libs/include2.h"
#include <stdio.h>
#include <stdlib.h>

#define NUM_MODULES 4

/*
temos uma grid de celulas, cada uma possuindo uma lista de 
modulos validos para essa posicao.
Cada modulo tem sua cor e lista de modulos adjacentes validos
*/








//Variaveis globais
int __num_cells_lowest_entropy;
int __lowest_entropy;   //menor entropia global
int __size_lin;
int __size_col;
int __num_modules;
struct Queue* __affected;  
struct node** __head = NULL;


/*Printa a grid para o terminal*/
void print_grid(Cell_2d** grid){
    //borda superior
    printf("\n");
    for(int i =0; i< __size_col*2+2; i++) printf("-");
    printf("\n");
    

    for(int i =__size_lin-1; i>=0; i--){
        printf("|");//borda

        for(int j =0; j< __size_col; j++){
            grid[i][j].entropy != 1? printf("%2d", grid[i][j].entropy): printf("%s%s", grid[i][j].modules[0]->color.a, grid[i][j].modules[0]->color.a); 
        }

        printf("|");//borda
        printf("\n");
    }

    //borda inferior
    for(int i =0; i< __size_col*2+2; i++) printf("-");
    printf("\n");
}


/*Percorre a grid, adicionando as celulas com menor entropia na lista delas*/
void backtrack(Cell_2d** grid){
    int entropy=__num_modules;
    for(int i =0; i<__size_lin; i++){
        for(int j =0; j<__size_col; j++){

            entropy = grid[i][j].entropy;

            if(entropy>1){
                if(entropy<__lowest_entropy){
                    clear(__head);
                    __num_cells_lowest_entropy = 0;
                    __lowest_entropy = entropy;
                }
                if(entropy==__lowest_entropy){
                    insertLast(__head, grid[i][j].pos);
                    __num_cells_lowest_entropy++;
                }
            }
        }
    }
}




//retorna um int entre 0 e ceiling incluso
int choose_rand(int ceiling){
    printf("Entrando choose_rand\n");
    return (int)(((float)rand()/(float)RAND_MAX)*(((float)ceiling)+0.99999)); //escolhe de 0..ceiling
}


void collapse_cell(Cell_2d* cell, int guess){
    printf("--------------------\n");
    printf("Entrando collpase_cell\n");
    printf("Cell lin: %d     col: %d\n", cell->pos.lin, cell->pos.col);
    int ceiling = cell->entropy;
    printf("ceiling_collapse, ceiling %d   -   guess:%d\n", ceiling, guess);
    for(int i=0; i<ceiling; i++){
        if(i!=guess){
            cell->modules[i] = NULL;
        }
    }

    cell->modules[0]=cell->modules[guess];
    if(guess!=0){cell->modules[guess]=NULL;}
    cell->entropy=1;
    printf("cell entropy: %d\n", cell->entropy);
    

    for(int i=0; i<ceiling; i++){
        printf("%p\n", cell->modules[i]);
    }
    printf("--------------------\n");
}





void constrain(Cell_2d* root, Cell_2d* target, char dir){
    printf("--------------------\n");
    printf("Entrando constrain com dir: %c\n", dir);
    int target_entropy = target->entropy;
    int root_entropy = root->entropy;
    int size_neig;

    printf("root_entropy: %d\n", root_entropy);

    //loopa pelos modulos de target, checando se sao validos
    
    for(int i =0; i < target_entropy; i++){
        printf("Entrando for i:%d\n", i);
        
        for(int j =0; j < root_entropy; j++){
            printf("Entrando for j:%d\n", j);
            switch (dir) {
                case 't' :{
                    printf("Pegando tamanho array - t\n");
                    //size_neig= root->modules[j]->sizeNeigTop;
                    size_neig = root[0].modules[j]->sizeNeigTop;
                    printf("peguei tam array\n");

                    for(int k=0;k<size_neig;k++){
                        //printf("Entrando for k:%d\n", k);
                        if(target->modules[i] == root->modules[j]->neigTop[k]){
                            printf("Goto\n");
                            goto continue_i;
                        }
                    }
                    break;
                }
                case 'b' :{
                    printf("Pegando tamanho array - b\n");
                    size_neig= root->modules[j]->sizeNeigBotton;
                    printf("peguei tam array\n");

                    for(int k=0;k<size_neig;k++){
                        //printf("Entrando for k\n");
                        if(target->modules[i] == root->modules[j]->neigBotton[k]){
                            printf("Goto\n");
                            goto continue_i;
                        }
                    }
                    break;
                }
                case 'r' :{
                    printf("Pegando tamanho array - r\n");
                    size_neig= root->modules[j]->sizeNeigRight;
                    printf("peguei tam array\n");

                    for(int k=0;k<size_neig;k++){
                        //printf("Entrando for k\n");
                        if(target->modules[i] == root->modules[j]->neigRight[k]){
                            printf("Goto\n");
                            goto continue_i;
                        }
                    }
                    break;
                }
                case 'l' :{
                    printf("Pegando tamanho array - l\n");
                    size_neig= root->modules[j]->sizeNeigLeft;
                    printf("peguei tam array\n");

                    for(int k=0;k<size_neig;k++){
                        //printf("Entrando for k\n");
                        if(target->modules[i] == root->modules[j]->neigLeft[k]){
                            printf("Goto\n");
                            goto continue_i;
                        }
                    }
                    break;
                }
            }
        }


        //remover modulo
        //printf("Removendo modulo\n");
        target->modules[i] = NULL;
        target->entropy--;

        continue_i:;
    }



    

    //checa se mudou alguma coisa, ordena, e coloca na __queue
    printf("Checando diminuicao em entropias adjacentes\n");
    printf("lin:%d  -  col: %d\n", target->pos.lin, target->pos.col);
    int end_entropy = target->entropy;
    printf("end_entropy: %d\n", end_entropy);

    printf("Checando checando se esta entre os com menos entropia\n");
    if(end_entropy>1){
        printf("if 1\n");
        printf("ee: %d   vs   le:%d\n", end_entropy, __lowest_entropy);
        if(end_entropy<__lowest_entropy || __num_cells_lowest_entropy==0){
            printf("entrei if 1\n");
            __lowest_entropy=end_entropy;
            clear(__head);
            __num_cells_lowest_entropy=0;
        }
        printf("if 2\n");
        if(end_entropy==__lowest_entropy){
            printf("entrei if 2\n");
            //target->pos = target->pos;
            //printf("test\n");
            printf("lin:%d  -  col: %d\n", target->pos.lin, target->pos.col);
            printf("%d\n", length(__head));
            if(insertLast_non_repete(__head, target->pos)){
                printf("entrei if 3\n");
                printf("entropy: %d\n", end_entropy);
                __num_cells_lowest_entropy++;//roda mais vezes __q devia? constrain eh pra rodar em entropia 1 tb  -  devia estar resolvido
                printf("ncle: %d   -   real: %d\n", __num_cells_lowest_entropy, length(__head));
            }
        }
    }

    if(end_entropy==1){
        struct node* temp = find_by_data(__head, target->pos);
        if(temp!=NULL){
            printf("\nERA PRA ESTAR RODANDO????\n\n");
            delete(__head, temp->key);
            __num_cells_lowest_entropy--;
        }
    }

    printf("Checando checando se foi modificado\n");
    if(end_entropy<target_entropy){
        

        enQueue(__affected, target->pos);

        for(int i = 0; i<target_entropy;i++){
            if(target->modules[i] != NULL){
                for(int j =0; j<i;j++){
                    if(target->modules[j]==NULL){
                        target->modules[j] = target->modules[i];
                        target->modules[i] = NULL;
                    }
                }
            }
        }
    }
    printf("Saindo constrain\n");
    printf("--------------------\n");
}


void propagate(Cell_2d** grid, Pos_2d root_pos){
    printf("--------------------\n");
    printf("Entrando propagate\n");
    Pos_2d target_pos;

    printf("root lin:%d  -  col:%d\n", root_pos.lin, root_pos.col);
    printf("root entropia:%d\n", grid[root_pos.lin][ root_pos.col].entropy);

    enQueue(__affected, root_pos);

    while(__affected->front!=NULL){
        target_pos = __affected->front->key;
        deQueue(__affected);
        //printf("target lin:%d  -  col:%d\n", target_pos.lin, target_pos.col);
        //printf("target entropia:%d\n", grid[target_pos.lin][target_pos.col].entropy);

        //ajeitar nao eh pra olhar 100% tudo em volta
        if(target_pos.lin<__size_lin-1/* && grid[target_pos.lin+1][target_pos.col].entropy>1*/){
            //printf("target lin:%d  -  col:%d\n", target_pos.lin, target_pos.col);
            //printf("target entropia:%d\n", grid[target_pos.lin][target_pos.col].entropy);
            constrain(&(grid[target_pos.lin][target_pos.col]), &(grid[target_pos.lin+1][target_pos.col]), 't');
        }
        if(target_pos.lin>0/* && grid[target_pos.lin-1][target_pos.col].entropy>1*/){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin-1][target_pos.col], 'b');
        }
        if(target_pos.col>0/* && grid[target_pos.lin][target_pos.col-1].entropy>1*/){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin][target_pos.col-1], 'l');
        }
        if(target_pos.col<__size_col-1/* && grid[target_pos.lin][target_pos.col+1].entropy>1*/){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin][target_pos.col+1], 'r');
        }

    }
    printf("--------------------\n");
}



/*Inicia o collapso de uma celula e chama as funções que calculam todos os efeitos colaterais*/
void collapse(Cell_2d** grid, Pos_2d pos){
    printf("Entrando collapse\n");

    

    Cell_2d* cell = &grid[pos.lin][pos.col];
    


    //nao pode chamar collapse em celulas com entropia menor __que 2
    if(cell->entropy <= 1) {  printf("Error: Tried to collapse cell with entropy of 1 or less\n"); exit(1);   }

    

    int ceiling = cell->entropy-1;
    /*SEMPRE 2*/
    printf("ceiling: %d\n", ceiling);
    int guess = choose_rand(ceiling);
    printf("guess: %d\n", guess);

    //torna nulos todas as referencias a modulos __que não foram a escolhida e traz a nao nula para frente
    /*CHECAR*/
    printf("antes collapse cell\n");
    printf("cell lin: %d   col: %d\n", cell->pos.lin, cell->pos.col);
    printf("cell entropy: %d\n", cell->entropy);
    collapse_cell(cell, guess);

    /*ATENCAO    -    ajeitar opcoes das celulas adjacentes*/
    /*CHECAR SE O SEG FAULT EH PQ DISSO AQ*/
    printf("antes propagate\n");
    printf("cell lin: %d   col: %d\n", cell->pos.lin, cell->pos.col);
    printf("cell entropy: %d\n", cell->entropy);
    printf("cell entropy na grid: %d\n", grid[cell->pos.lin][cell->pos.col].entropy);
    propagate(grid, cell->pos);
}


Cell_2d** inputFile(char* adress){

    __size_lin = 5;
    __size_col = 5;

    Cell_2d** grid;
    return grid;
}


Cell_2d** setupTest(){
    Cell_2d** grid;


    __num_modules = NUM_MODULES;
    srand ( time(NULL) );
    
    __lowest_entropy=__num_modules;
    __num_cells_lowest_entropy = 0;

    __size_lin = 5;
    __size_col = 5;

    __affected = createQueue();
    

    printf("Aloca modulos\n");
    Module_2d* modulos = (Module_2d*)malloc(sizeof(Module_2d)*__num_modules);
    for(int i = 0; i<__num_modules; i++){
        modulos[i].neigBotton = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigTop = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigLeft = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigRight = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        for(int j=0; j<__num_modules; j++){
            modulos[i].neigBotton[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigTop[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigLeft[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigRight[j] = (Module_2d*)malloc(sizeof(Module_2d));
        }
    }


    char* A = "▓";
    char* B = "█";
    char* C = "░";
    char* D = " ";
    modulos[0].color.a =   A;
    modulos[1].color.a =   B;
    modulos[2].color.a =   C;
    modulos[3].color.a =   D;
    
    
    //regras
    printf("Seta regras\n");
    modulos[0].neigBotton[0]=&modulos[0];
    modulos[0].sizeNeigBotton=1;

    modulos[0].neigTop[0]=&modulos[0];
    modulos[0].neigTop[1]=&modulos[1];
    modulos[0].neigTop[2]=&modulos[3];
    modulos[0].sizeNeigTop=3;

    modulos[0].neigLeft[0]=&modulos[0];
    modulos[0].neigLeft[1]=&modulos[3];
    modulos[0].sizeNeigLeft=2;

    modulos[0].neigRight[0]=&modulos[0];
    modulos[0].neigRight[1]=&modulos[3];
    modulos[0].sizeNeigRight=2;



    modulos[1].neigBotton[0]=&modulos[0];
    modulos[1].neigBotton[1]=&modulos[1];
    modulos[1].sizeNeigBotton=2;

    modulos[1].neigTop[0]=&modulos[1];
    modulos[1].neigTop[1]=&modulos[2];
    modulos[1].sizeNeigTop=2;

    modulos[1].neigLeft[0]=&modulos[3];
    modulos[1].sizeNeigLeft=1;

    modulos[1].neigRight[0]=&modulos[3];
    modulos[1].sizeNeigRight=1;




    modulos[2].neigBotton[0]=&modulos[1];
    modulos[2].neigBotton[1]=&modulos[2];
    modulos[2].neigBotton[2]=&modulos[3];
    modulos[2].sizeNeigBotton=3;

    modulos[2].neigTop[0]=&modulos[2];
    modulos[2].neigTop[1]=&modulos[3];
    modulos[2].sizeNeigTop=2;

    modulos[2].neigLeft[0]=&modulos[2];
    modulos[2].neigLeft[1]=&modulos[3];
    modulos[2].sizeNeigLeft=2;

    modulos[2].neigRight[0]=&modulos[2];
    modulos[2].neigRight[1]=&modulos[3];
    modulos[2].sizeNeigRight=2;





    modulos[3].neigBotton[0]=&modulos[0];
    modulos[3].neigBotton[1]=&modulos[2];
    modulos[3].neigBotton[2]=&modulos[3];
    modulos[3].sizeNeigBotton=3;

    modulos[3].neigTop[0]=&modulos[2];
    modulos[3].neigTop[1]=&modulos[3];
    modulos[3].sizeNeigTop=2;

    modulos[3].neigLeft[0]=&modulos[0];
    modulos[3].neigLeft[1]=&modulos[1];
    modulos[3].neigLeft[2]=&modulos[2];
    modulos[3].neigLeft[3]=&modulos[3];
    modulos[3].sizeNeigLeft=__num_modules;

    modulos[3].neigRight[0]=&modulos[0];
    modulos[3].neigRight[1]=&modulos[1];
    modulos[3].neigRight[2]=&modulos[2];
    modulos[3].neigRight[3]=&modulos[3];
    modulos[3].sizeNeigRight=__num_modules;


    for(int i = 0;i < __num_modules; i++){
        printf("modulo %d\n", i);

        printf("size: %d\n", modulos[i].sizeNeigBotton);
        for(int j = 0; j< modulos[i].sizeNeigBotton; j++){
            printf("%c|", *modulos[i].neigBotton[j]->color.a);
        }
        printf("\n");

        printf("size: %d\n", modulos[i].sizeNeigTop);
        for(int j = 0; j< modulos[i].sizeNeigTop; j++){
            printf("%c|", *modulos[i].neigTop[j]->color.a);
        }
        printf("\n");

        printf("size: %d\n", modulos[i].sizeNeigLeft);
        for(int j = 0; j< modulos[i].sizeNeigLeft; j++){
            printf("%c|", *modulos[i].neigLeft[j]->color.a);
        }
        printf("\n");

        printf("size: %d\n", modulos[i].sizeNeigRight);
        for(int j = 0; j< modulos[i].sizeNeigRight; j++){
            printf("%c|", *modulos[i].neigRight[j]->color.a);
        }
        printf("\n");

        printf("\n");
    }




    //aloca grid
    printf("Aloca grid\n");
    grid = (Cell_2d**)malloc(sizeof(Cell_2d*)*__size_lin);
    for(int i =0; i<__size_lin; i++){
        grid[i]=(Cell_2d*)malloc(sizeof(Cell_2d)*__size_col);
        for(int j =0; j<__size_col; j++){
            grid[i][j].modules = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
            grid[i][j].modules[0] = (Module_2d*)malloc(sizeof(Module_2d));
            grid[i][j].modules[1] = (Module_2d*)malloc(sizeof(Module_2d));
            grid[i][j].modules[2] = (Module_2d*)malloc(sizeof(Module_2d));
            grid[i][j].modules[3] = (Module_2d*)malloc(sizeof(Module_2d));
        }
    }

    //preenche grid
    printf("Preenche grid\n");
    for(int i = 0; i<__size_lin; i++){
        for(int j = 0; j<__size_col; j++){
            grid[i][j].pos.lin = i; 
            grid[i][j].pos.col = j;
            grid[i][j].entropy = __num_modules;
            

            grid[i][j].modules[0] = &(modulos[0]);
            grid[i][j].modules[1] = &(modulos[1]);
            grid[i][j].modules[2] = &(modulos[2]);
            grid[i][j].modules[3] = &(modulos[3]);
        }
    }









    return grid;
}

/*Wave Function Collapse.
Recebe o endereço de um arquivo de entrada, contendo os modulos possiveis, as regras e o tamanho desejado da grid.*/
void wfc(char* adress){

    //seed do rand()
    srand ( time(NULL) );
    
    __lowest_entropy=__num_modules;
    __num_cells_lowest_entropy = 0;



    __affected = createQueue();
    __head = (struct node**)malloc(sizeof(struct node*));
    __head[0] = NULL;


    //Cria e preenche a grid
    Cell_2d** grid;
    //grid = inputFile(adress);
    grid = setupTest(); //teste base

    //posicao randomica na lista de celulas de menor entropia
    int rand_cell=0;

    //o node retirado da lista de celulas de menor entropia
    struct node* next_node;

    //proxima posicao a ser collapsada
    Pos_2d next;
    next.lin = choose_rand(__size_lin-1);
    next.col = choose_rand(__size_col-1);

    //primeira inserção na lista
    printf("primeiro insere\n");
    insertLast_non_repete(__head, next);
    __num_cells_lowest_entropy=1;
    printf("terminei primeiro insere\n");

    while(!isEmpty(__head)){
        printf("loop iter\n");
        //deletando a proxima celula a ser collapsada da lista
        delete(__head, rand_cell);
        __num_cells_lowest_entropy--;

        //collapsando celula
        collapse(grid, next);

        //se a lista de celulas de menor entropia esta vazia, faz uma varredura da grid por celulas nao collapsadas
        if(__num_cells_lowest_entropy == 0){
            __lowest_entropy = __num_modules;
            backtrack(grid);
        }


        //proxima celula a ser collapsada
        rand_cell = choose_rand(__num_cells_lowest_entropy-1);

       
       //node da proxima celula a ser collapsada
        next_node = find(__head, rand_cell); 
        if(next_node==NULL){
            break;
        }

        //atualizando next
        next = next_node->data;

        print_grid(grid);

    }
    print_grid(grid);
}





int main(void){
    wfc("");
}
#include <stdlib.h>


//Variaveis globais
extern int __num_cells_lowest_entropy;
extern int __lowest_entropy;   //menor entropia global

extern int __size_lin;
extern int __size_col;

extern int __num_modules;

extern struct Queue* __affected_cells;  
extern struct node** __lowest_entropy_cells;


/*
Retorna um int entre 0 e ceiling incluso
*/
int choose_rand(int ceiling){
    return (int)(((float)rand()/(float)RAND_MAX)*(((float)ceiling)+0.99999)); //escolhe de [0..ceiling]
}
/*
Recebe o endereco do arquivo de entrada, o le, registra o numero de modulos e suas regras, e inicializa a grid.
*/
Cell_2d** setup(char* adress){

    FILE *fp;              
	char buffer[1024];
	struct json_object *parsed_json;    //blibioteca json-c.lib que faz parser(pesquisar) do arquivo texto em objeto
	int i, j, k, sizeNeig;    
    json_object *listaModulosObj, *moduloObj, *valorObj, *valorListaObj, *listaAtributosObj; //objetos de navegação no json
  
    Cell_2d** grid;

    //seed do rand()
    srand ( time(NULL) );
        
    __affected_cells = createQueue();
    
    __num_cells_lowest_entropy = 0;

    // Lê arquivo texto setup.json
	fp = fopen(adress, "r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);

    // Transforma o arquivo texto .json em Objeto
	parsed_json = json_tokener_parse(buffer);

    // Procura no Objeto Json o atributo "grid" que contém o número de linhas e colunas
    if (json_object_object_get_ex(parsed_json, "grid", &listaAtributosObj) == false) {
        printf( "Query Modulos ERRO\n" );
        return NULL;
    }

    // Obtém size-lin x size-col
    if (json_object_object_get_ex(listaAtributosObj, "size-lin", &valorObj )) {
        __size_lin = atoi(json_object_to_json_string( valorObj)); //converte o valor do objeto encontrado em string para depois converter para inteiro
        printf("LINHAS==>%d\n", __size_lin);
    }
    if (json_object_object_get_ex(listaAtributosObj, "size-col", &valorObj )) {
        __size_col = atoi(json_object_to_json_string( valorObj)); //converte o valor do objeto encontrado em string para depois converter para inteiro
        printf("COLUNAS==>%d\n", __size_col);
    }

//-------------------------

    // Procura no Objeto Json o atributo "modulos" que define as regras
    if (json_object_object_get_ex(parsed_json, "modulos", &listaModulosObj) == false) {
        printf( "Query Modulos ERRO\n" );
        return NULL;
    }

    __num_modules = json_object_array_length( listaModulosObj ); //numero de modulos
    __lowest_entropy=__num_modules;   //menor entropia inicial = numero de modulos


    //alocação dos módulos
    printf("Aloca %d modulos \n", __num_modules);
    Module_2d* modulos = (Module_2d*)malloc(sizeof(Module_2d)*__num_modules);
    for(int i = 0; i<__num_modules; i++){
        modulos[i].neigBotton = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigTop = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigLeft = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigRight = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        for(int j=0; j<__num_modules; j++){
            modulos[i].neigBotton[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigTop[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigLeft[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigRight[j] = (Module_2d*)malloc(sizeof(Module_2d));
        }
    }

    /* LOOP em cada Módulo para atribuir as regras */
    for ( i = 0; i < __num_modules; i++ ) {

        moduloObj = json_object_array_get_idx( listaModulosObj, i ); //funcao para andar dentro dos arrays que representam as regras

        //inicialização dos números de elementos dos arrays que representam as regras
        modulos[i].sizeNeigBotton = 0;
        modulos[i].sizeNeigTop = 0;
        modulos[i].sizeNeigLeft = 0;
        modulos[i].sizeNeigRight = 0;

        // COR
        if (json_object_object_get_ex( moduloObj, "cor", &valorObj )) {
            modulos[i].color.a = (char *) json_object_to_json_string( valorObj ); //converte o valor do objeto encontrado em string para 
        }                                                                         //depois converter para char pointer
        
        // neigBotton        
        if (json_object_object_get_ex( moduloObj, "neigBotton", &valorListaObj )) { //obtem o array de índices dos módulos do botton

            sizeNeig = json_object_array_length( valorListaObj );
            modulos[i].sizeNeigBotton = sizeNeig;

            for ( j = 0; j < sizeNeig; j++ ) {
                valorObj = json_object_array_get_idx (valorListaObj, j);
                modulos[i].neigBotton[j] = &modulos[atoi(json_object_to_json_string( valorObj ))]; //atribui a regra
            }
        }

        // neigTop        
        if (json_object_object_get_ex( moduloObj, "neigTop", &valorListaObj )) { //obtem o array de índices dos módulos do top
            
            sizeNeig = json_object_array_length( valorListaObj );
            modulos[i].sizeNeigTop = sizeNeig;

            for ( j = 0; j < sizeNeig; j++ ) {
                valorObj = json_object_array_get_idx (valorListaObj, j);
                modulos[i].neigTop[j] = &modulos[atoi(json_object_to_json_string( valorObj ))]; //atribui a regra
            }
        }

        // neigLeft
        if (json_object_object_get_ex( moduloObj, "neigLeft", &valorListaObj )) { //obtem o array de índices dos módulos do left
            
            sizeNeig = json_object_array_length( valorListaObj );
            modulos[i].sizeNeigLeft = sizeNeig;

            for ( j = 0; j < sizeNeig; j++ ) {
                valorObj = json_object_array_get_idx (valorListaObj, j);
                modulos[i].neigLeft[j] = &modulos[atoi(json_object_to_json_string( valorObj ))]; //atribui a regra
            }
        }

        // neigRight
        if (json_object_object_get_ex( moduloObj, "neigRight", &valorListaObj )) { //obtem o array de índices dos módulos do right
            
            sizeNeig = json_object_array_length( valorListaObj );
            modulos[i].sizeNeigRight = sizeNeig;

            for ( j = 0; j < sizeNeig; j++ ) {
                valorObj = json_object_array_get_idx (valorListaObj, j);
                modulos[i].neigRight[j] = &modulos[atoi(json_object_to_json_string( valorObj ))]; //atribui a regra
            }
        }

    }

    // TESTE
    printf("\n");
    for(int i = 0;i < __num_modules; i++){
        printf("Modulo %d\n", i);

        printf("size B: %d\n", modulos[i].sizeNeigBotton);
        for(int j = 0; j< modulos[i].sizeNeigBotton; j++){
            printf("%s|", modulos[i].neigBotton[j]->color.a);
        }
        printf("\n");

        printf("size T: %d\n", modulos[i].sizeNeigTop);
        for(int j = 0; j< modulos[i].sizeNeigTop; j++){
            printf("%s|", modulos[i].neigTop[j]->color.a);
        }
        printf("\n");

        printf("size L: %d\n", modulos[i].sizeNeigLeft);
        for(int j = 0; j< modulos[i].sizeNeigLeft; j++){
            printf("%s|", modulos[i].neigLeft[j]->color.a);
        }
        printf("\n");

        printf("size R: %d\n", modulos[i].sizeNeigRight);
        for(int j = 0; j< modulos[i].sizeNeigRight; j++){
            printf("%s|", modulos[i].neigRight[j]->color.a);
        }
        printf("\n");

        printf("\n");
    }

    // Aloca Grid

    printf("Aloca grid\n");

    grid = (Cell_2d**)malloc(sizeof(Cell_2d*)*__size_lin);

    for(int i =0; i<__size_lin; i++){

        grid[i]=(Cell_2d*)malloc(sizeof(Cell_2d)*__size_col);

        for(int j =0; j<__size_col; j++){
            
            grid[i][j].modules = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);

            for(int k=0; k<__num_modules; k++) {
                grid[i][j].modules[k] = (Module_2d*)malloc(sizeof(Module_2d));
            }
        }
    }

    // Preenche Grid
    printf("Preenche grid\n");

    for(int i = 0; i<__size_lin; i++){

        for(int j = 0; j<__size_col; j++){
            
            grid[i][j].pos.lin = i; 
            grid[i][j].pos.col = j;
            grid[i][j].entropy = __num_modules;
            
            for(int k=0; k<__num_modules; k++) {
                grid[i][j].modules[k] = &(modulos[k]);                

            }
        }
    }


    return grid;
}
#include "../libs/include.h"



/*
temos uma grid de celulas, cada uma possuindo uma lista de 
modulos validos para essa posicao.
Cada modulo tem sua cor e lista de modulos adjacentes validos
*/








//cor de um modulo




int size;
struct Queue* q;


int choose_module_rand(int ceiling){
    return (int)(((float)rand()/(float)RAND_MAX)*ceiling); //escolhe de 0..ceiling
}


void collapse_cell(Cell_2d* cell, int guess){
    
    int ceiling = cell->entropy-1;

    for(int i=0; i<ceiling; i++){
        if(i!=guess){
            cell->modules[i] = NULL;
        }
    }

    cell->modules[0]=cell->modules[guess];
    cell->modules[guess]=NULL;
    cell->entropy=1;
}



/*
void propagate(Cell_2d** grid, Pos_2d root_pos){
    Pos_2d target_pos;

    if(!(root_pos.lin < 0) && !(root_pos.lin > size-1) && !(root_pos.col < 0) && !(root_pos.col > size-1)){

    }
}
*/

void checkValidity(char dir){
    switch (dir) {
    case 't' :{
        
        break;
    }
    case 'b' :{

        break;
    }
    case 'r' :{

        break;
    }
    case 'l' :{

        break;
    }
    }
}

void constrain(Cell_2d* root, Cell_2d* target, char dir){
    int target_entropy = target->entropy;
    int root_entropy = root->entropy;
    int size_neig;

    //loopa pelos modulos de target, checando se sao validos
    for(int i =0; i < target_entropy; i++){
        for(int j =0; j < root_entropy; j++){
            switch (dir) {
                case 't' :{
                    size_neig= root->modules[j]->sizeNeigTop;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == root->modules[j]->neigTop[k]){
                            goto continue_i;
                        }
                    }
                    break;
                }
                case 'b' :{
                    size_neig= root->modules[j]->sizeNeigBotton;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == root->modules[j]->neigBotton[k]){
                            goto continue_i;
                        }
                    }
                    break;
                }
                case 'r' :{
                    size_neig= root->modules[j]->sizeNeigRight;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == root->modules[j]->neigRight[k]){
                            goto continue_i;
                        }
                    }
                    break;
                }
                case 'l' :{
                    size_neig= root->modules[j]->sizeNeigLeft;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == root->modules[j]->neigLeft[k]){
                            goto continue_i;
                        }
                    }
                    break;
                }
            }
        }


        //remover modulo
        target->modules[i] = NULL;
        target->entropy--;

        continue_i:;
    }



    

    //checa se mudou alguma coisa, ordena, e coloca na queue

    int end_entropy = target->entropy;

    if(end_entropy<target_entropy){

        enQueue(q, target->pos);

        for(int i = 0; i<target_entropy;i++){
            if(target->modules[i] != NULL){
                for(int j =0; j<i;j++){
                    if(target->modules[j]==NULL){
                        target->modules[j] = target->modules[i];
                        target->modules[i] = NULL;
                    }
                }
            }
        }
    }
}


void propagate(Cell_2d** grid, Pos_2d root_pos){
    Pos_2d target_pos;

    enQueue(q, root_pos);

    while(q->front!=NULL){
        target_pos = q->front->key;
        deQueue(q);


        //ajeitar nao eh pra olhar 100% tudo em volta
        if(target_pos.lin<size-1 && grid[target_pos.lin+1][target_pos.col].entropy>1){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin+1][target_pos.col], 't');
        }
        if(target_pos.lin>0 && grid[target_pos.lin-1][target_pos.col].entropy>1){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin-1][target_pos.col], 'b');
        }
        if(target_pos.col>0 && grid[target_pos.lin][target_pos.col-1].entropy>1){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin][target_pos.col-1], 'l');
        }
        if(target_pos.col<size-1 && grid[target_pos.lin][target_pos.col+1].entropy>1){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin][target_pos.col+1], 'r');
        }

    }
}




void collapse(Cell_2d** grid, Pos_2d pos){

    Cell_2d cell = grid[pos.lin][pos.col];
    


    //nao pode chamar collapse em celulas com entropia menor que 2
    if(cell.entropy <= 1) {  printf("Error: Tried to collapse cell with entropy of 1 or less"); exit(1);   }

    

    int ceiling = cell.entropy-1;
    int guess = choose_module_rand(ceiling);

    //torna nulos todas as referencias a modulos que não foram a escolhida e traz a nao nula para frente
    collapse_cell(&cell, guess);

    /*ATENCAO    -    ajeitar opcoes das celulas adjacentes*/
    propagate(grid, cell.pos);
}








int main(void){
    size = 5;
    Cell_2d** grid;

    q = createQueue();
    //enQueue(q, 10);
    //deQueue(q);
    //queue-> front pra acessar

    Color color_A;
    color_A.a='A';
    Color color_B;
    color_B.a='B';
    Color color_C;
    color_C.a='C';
    Color color_D;
    color_D.a='D';

    Module_2d* modulos = (Module_2d*)malloc(sizeof(Module_2d)*size);

    Module_2d mod_A;
    mod_A.color=color_A;
    
    Module_2d mod_B;
    mod_B.color=color_B;

    Module_2d mod_C;
    mod_C.color=color_C;

    Module_2d mod_D;
    mod_D.color=color_D;

    //A=chao    B=tronco    C=folhas   D=ceu


    //regras
    mod_A.neigBotton[0]=&mod_A;
    mod_A.sizeNeigBotton=1;

    mod_A.neigTop[0]=&mod_A;
    mod_A.neigTop[1]=&mod_B;
    mod_A.neigTop[2]=&mod_D;
    mod_A.sizeNeigTop=3;

    mod_A.neigLeft[0]=&mod_A;
    mod_A.neigLeft[1]=&mod_D;
    mod_A.sizeNeigLeft=2;

    mod_A.neigRight[0]=&mod_A;
    mod_A.neigRight[1]=&mod_D;
    mod_A.sizeNeigRight=2;



    mod_B.neigBotton[0]=&mod_A;
    mod_B.neigBotton[1]=&mod_B;
    mod_B.sizeNeigBotton=2;

    mod_B.neigTop[0]=&mod_B;
    mod_B.neigTop[1]=&mod_C;
    mod_B.sizeNeigTop=2;

    mod_B.neigLeft[0]=&mod_D;
    mod_B.sizeNeigLeft=1;

    mod_B.neigRight[0]=&mod_D;
    mod_B.sizeNeigRight=1;




    mod_C.neigBotton[0]=&mod_B;
    mod_C.neigBotton[0]=&mod_C;
    mod_C.sizeNeigBotton=2;

    mod_C.neigTop[0]=&mod_C;
    mod_C.neigTop[1]=&mod_D;
    mod_C.sizeNeigTop=2;

    mod_C.neigLeft[0]=&mod_C;
    mod_C.neigLeft[1]=&mod_D;
    mod_C.sizeNeigLeft=2;

    mod_C.neigRight[0]=&mod_C;
    mod_C.neigRight[1]=&mod_D;
    mod_C.sizeNeigRight=2;





    mod_D.neigBotton[0]=&mod_A;
    mod_D.neigBotton[1]=&mod_C;
    mod_D.neigBotton[2]=&mod_D;
    mod_D.sizeNeigBotton=3;

    mod_D.neigTop[0]=&mod_D;
    mod_D.sizeNeigTop=1;

    mod_D.neigLeft=modulos;
    mod_D.sizeNeigLeft=4;

    mod_D.neigRight=modulos;
    mod_D.sizeNeigRight=4;





    //checar se ta funcionando
    modulos[0]=mod_A;
    modulos[1]=mod_B;
    modulos[2]=mod_C;
    modulos[3]=mod_D;


    //aloca grid
    grid = (Cell_2d**)malloc(sizeof(Cell_2d*)*size);
    for(int i =0; i<size; i++){
        grid[i]=(Cell_2d*)malloc(sizeof(Cell_2d)*size);
    }

    //preenche grid
    for(int i = 0; i<size; i++){
        for(int j = 0; j<size; j++){
            grid[i][j].pos.lin = i; 
            grid[i][j].pos.col = j;
            grid[i][j].entropy = 4;
            grid[i][j].modules = modulos;
        }
    }
}
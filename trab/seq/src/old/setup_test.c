Cell_2d** setup_test(){
    Cell_2d** grid;


    __num_modules = NUM_MODULES;
    srand ( time(NULL) );
    
    __lowest_entropy=__num_modules;
    __num_cells_lowest_entropy = 0;

    __size_lin = 15;
    __size_col = 15;

    __affected_cells = createQueue();
    

    printf("Aloca modulos\n");
    Module_2d* modulos = (Module_2d*)malloc(sizeof(Module_2d)*__num_modules);
    for(int i = 0; i<__num_modules; i++){
        modulos[i].neigBotton = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigTop = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigLeft = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigRight = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        for(int j=0; j<__num_modules; j++){
            modulos[i].neigBotton[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigTop[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigLeft[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigRight[j] = (Module_2d*)malloc(sizeof(Module_2d));
        }
    }


    char* A = "▓";
    char* B = "█";
    char* C = "░";
    char* D = " ";
    modulos[0].color.a =   A;
    modulos[1].color.a =   B;
    modulos[2].color.a =   C;
    modulos[3].color.a =   D;
    
    
    //regras
    printf("Seta regras\n");
    modulos[0].neigBotton[0]=&modulos[0];
    modulos[0].sizeNeigBotton=1;

    modulos[0].neigTop[0]=&modulos[0];
    modulos[0].neigTop[1]=&modulos[1];
    modulos[0].neigTop[2]=&modulos[3];
    modulos[0].sizeNeigTop=3;

    modulos[0].neigLeft[0]=&modulos[0];
    modulos[0].neigLeft[1]=&modulos[3];
    modulos[0].sizeNeigLeft=2;

    modulos[0].neigRight[0]=&modulos[0];
    modulos[0].neigRight[1]=&modulos[3];
    modulos[0].sizeNeigRight=2;



    modulos[1].neigBotton[0]=&modulos[0];
    modulos[1].neigBotton[1]=&modulos[1];
    modulos[1].sizeNeigBotton=2;

    modulos[1].neigTop[0]=&modulos[1];
    modulos[1].neigTop[1]=&modulos[2];
    modulos[1].sizeNeigTop=2;

    modulos[1].neigLeft[0]=&modulos[3];
    modulos[1].sizeNeigLeft=1;

    modulos[1].neigRight[0]=&modulos[3];
    modulos[1].sizeNeigRight=1;




    modulos[2].neigBotton[0]=&modulos[1];
    modulos[2].neigBotton[1]=&modulos[2];
    modulos[2].neigBotton[2]=&modulos[3];
    modulos[2].sizeNeigBotton=3;

    modulos[2].neigTop[0]=&modulos[2];
    modulos[2].neigTop[1]=&modulos[3];
    modulos[2].sizeNeigTop=2;

    modulos[2].neigLeft[0]=&modulos[2];
    modulos[2].neigLeft[1]=&modulos[3];
    modulos[2].sizeNeigLeft=2;

    modulos[2].neigRight[0]=&modulos[2];
    modulos[2].neigRight[1]=&modulos[3];
    modulos[2].sizeNeigRight=2;





    modulos[3].neigBotton[0]=&modulos[0];
    modulos[3].neigBotton[1]=&modulos[2];
    modulos[3].neigBotton[2]=&modulos[3];
    modulos[3].sizeNeigBotton=3;

    modulos[3].neigTop[0]=&modulos[2];
    modulos[3].neigTop[1]=&modulos[3];
    modulos[3].sizeNeigTop=2;

    modulos[3].neigLeft[0]=&modulos[0];
    modulos[3].neigLeft[1]=&modulos[1];
    modulos[3].neigLeft[2]=&modulos[2];
    modulos[3].neigLeft[3]=&modulos[3];
    modulos[3].sizeNeigLeft=__num_modules;

    modulos[3].neigRight[0]=&modulos[0];
    modulos[3].neigRight[1]=&modulos[1];
    modulos[3].neigRight[2]=&modulos[2];
    modulos[3].neigRight[3]=&modulos[3];
    modulos[3].sizeNeigRight=__num_modules;


    printf("\n");
    for(int i = 0;i < __num_modules; i++){
        printf("Modulo %d\n", i);

        printf("size B: %d\n", modulos[i].sizeNeigBotton);
        for(int j = 0; j< modulos[i].sizeNeigBotton; j++){
            printf("%s|", modulos[i].neigBotton[j]->color.a);
        }
        printf("\n");

        printf("size T: %d\n", modulos[i].sizeNeigTop);
        for(int j = 0; j< modulos[i].sizeNeigTop; j++){
            printf("%s|", modulos[i].neigTop[j]->color.a);
        }
        printf("\n");

        printf("size L: %d\n", modulos[i].sizeNeigLeft);
        for(int j = 0; j< modulos[i].sizeNeigLeft; j++){
            printf("%s|", modulos[i].neigLeft[j]->color.a);
        }
        printf("\n");

        printf("size R: %d\n", modulos[i].sizeNeigRight);
        for(int j = 0; j< modulos[i].sizeNeigRight; j++){
            printf("%s|", modulos[i].neigRight[j]->color.a);
        }
        printf("\n");

        printf("\n");
    }




    //aloca grid
    printf("Aloca grid\n");
    grid = (Cell_2d**)malloc(sizeof(Cell_2d*)*__size_lin);
    for(int i =0; i<__size_lin; i++){
        grid[i]=(Cell_2d*)malloc(sizeof(Cell_2d)*__size_col);
        for(int j =0; j<__size_col; j++){
            grid[i][j].modules = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
            grid[i][j].modules[0] = (Module_2d*)malloc(sizeof(Module_2d));
            grid[i][j].modules[1] = (Module_2d*)malloc(sizeof(Module_2d));
            grid[i][j].modules[2] = (Module_2d*)malloc(sizeof(Module_2d));
            grid[i][j].modules[3] = (Module_2d*)malloc(sizeof(Module_2d));
        }
    }

    //preenche grid
    printf("Preenche grid\n");
    for(int i = 0; i<__size_lin; i++){
        for(int j = 0; j<__size_col; j++){
            grid[i][j].pos.lin = i; 
            grid[i][j].pos.col = j;
            grid[i][j].entropy = __num_modules;
            

            grid[i][j].modules[0] = &(modulos[0]);
            grid[i][j].modules[1] = &(modulos[1]);
            grid[i][j].modules[2] = &(modulos[2]);
            grid[i][j].modules[3] = &(modulos[3]);
        }
    }



    return grid;
}
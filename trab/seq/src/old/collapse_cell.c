#include "../libs/structs.h"
#include "stdlib.h"

//Variaveis globais
extern int __num_cells_lowest_entropy;
extern int __lowest_entropy;   //menor entropia global

extern int __size_lin;
extern int __size_col;

extern int __num_modules;

extern struct Queue* __affected_cells;  
extern struct node** __lowest_entropy_cells;

/*
Colapsa uma celula e reordena seu modulo restante
*/
void collapse_cell(Cell_2d* cell, int guess){
    int ceiling = cell->entropy;

    for(int i=0; i<ceiling; i++){
        if(i!=guess){
            cell->modules[i] = NULL;
        }
    }

    cell->modules[0]=cell->modules[guess];
    if(guess!=0){cell->modules[guess]=NULL;}
    cell->entropy=1;
}
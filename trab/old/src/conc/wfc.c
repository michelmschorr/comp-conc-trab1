#include "../../libs/conc/include.h"


//numero de modulos para o teste
#define NUM_MODULES 4





//Variaveis globais
int __num_cells_lowest_entropy; //numero de celulas de menor entropia
int __lowest_entropy;   //menor entropia global

int __size_lin;     //numero de linhas da grid
int __size_col;     //numero de colunas da grid

int __num_modules;  //numero de modulos

int __active_threads;   //numero de threads realizando propagacao
int __NTHREADS;         //numero de threads
bool __propagating=false;   //se a propagacao esta em progresso


struct Queue* __affected_cells;       //queue das proximas celulas a realizar a propagacao (apos um colapso)
struct node** __lowest_entropy_cells = NULL;    //lista de celulas de menor entropia


pthread_cond_t __cond_propagate;    //controla a entrada de threads propagadoras
pthread_cond_t __cond_occupied;     //controla se a celula origin da propagacao (ou suas adjacentes) estao ocupadas, e se precisam esperar
pthread_cond_t __cond_collapse;     //controla quando comeca o proximo colapso, esperando a propagacao acabar

pthread_mutex_t __lock_occupied;    //controla secoes criticas onde ha acesso a variavel ocupied das celulas
pthread_mutex_t __lock_lowest_entropy;  //controla secoes criticas onde ha acesso a lista de menor entropia ou suas variaveis de controle





/*
Printa a grid para o terminal
*/
void print_grid(Cell_2d** grid){
    //borda superior
    for(int i =0; i< __size_col*2+2; i++) printf("-");
    printf("\n");
    

    for(int i =__size_lin-1; i>=0; i--){
        printf("|");//borda esquerda

        for(int j =0; j< __size_col; j++){
            grid[i][j].entropy != 1? printf("%2d", grid[i][j].entropy): printf("%s%s", grid[i][j].modules[0]->color.a, grid[i][j].modules[0]->color.a); 
        }

        printf("|");//borda direita
        printf("\n");
    }

    //borda inferior
    for(int i =0; i< __size_col*2+2; i++) printf("-");
    printf("\n");
    printf("\n");
}







/*
Percorre a grid, adicionando as celulas com menor entropia na lista delas
*/
void backtrack(Cell_2d** grid){
 
    printf("backtrack\n");
    printf("ncle pre backtrack: %d\n", __num_cells_lowest_entropy);
    print_grid(grid);

    /*
    compara a entropia de cada celula que percorre com a menor_entropia(__lowest_entropy)
    caso menor, a substitui e limpa a lista de menores_entropias, 
    caso igual, adiciona a celula a lista
    */
    int entropy=__num_modules;
    for(int i =0; i<__size_lin; i++){
        for(int j =0; j<__size_col; j++){

            entropy = grid[i][j].entropy;

            if(entropy>1){
                if(entropy<__lowest_entropy){
                    clear(__lowest_entropy_cells);
                    __num_cells_lowest_entropy = 0;
                    __lowest_entropy = entropy;
                }
                if(entropy==__lowest_entropy){
                    insertLast(__lowest_entropy_cells, grid[i][j].pos);
                    __num_cells_lowest_entropy++;
                }
            }
        }
    }
    printf("ncle pos backtrack: %d\n", __num_cells_lowest_entropy);
}






/*
Retorna um int entre 0 e ceiling incluso
*/
int choose_rand(int ceiling){
    return (int)(((float)rand()/(float)RAND_MAX)*(((float)ceiling)+0.99999)); //escolhe de [0..ceiling]
}


/*
Colapsa uma celula e reordena seu modulo restante
*/
void collapse_cell(Cell_2d* cell, int guess){
    int ceiling = cell->entropy;

    for(int i=0; i<ceiling; i++){
        if(i!=guess){
            cell->modules[i] = NULL;
        }
    }

    cell->modules[0]=cell->modules[guess];
    if(guess!=0){cell->modules[guess]=NULL;}
    cell->entropy=1;
}




/*
Remove da celula target qualquer modulo que nao seja permitido pela celula origin,
pode adicionar target tanto a queue de propagacao caso tenha alterado sua entropia,
quanto a lista de menor entropia, caso esteja agora entre elas
*/
void constrain(Cell_2d* origin, Cell_2d* target, char dir){
    int target_entropy = target->entropy;
    int origin_entropy = origin->entropy;
    int size_neig;


    /*
    loopa pelos modulos de target, checando se sao validos,
    loopando pela lista de modulos validos do lado correspondente de cada
    modulo possivel de origin
    */
    for(int i =0; i < target_entropy; i++){

        for(int j =0; j < origin_entropy; j++){

            switch (dir) {
                //casa com a direcao recebida
                case 't' :{
                    size_neig = origin[0].modules[j]->sizeNeigTop;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == origin->modules[j]->neigTop[k]){
                            goto valid_module;
                        }
                    }
                    break;
                }
                case 'b' :{
                    size_neig= origin->modules[j]->sizeNeigBotton;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == origin->modules[j]->neigBotton[k]){
                            goto valid_module;
                        }
                    }
                    break;
                }
                case 'r' :{
                    size_neig= origin->modules[j]->sizeNeigRight;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == origin->modules[j]->neigRight[k]){
                            goto valid_module;
                        }
                    }
                    break;
                }
                case 'l' :{
                    size_neig= origin->modules[j]->sizeNeigLeft;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == origin->modules[j]->neigLeft[k]){
                            goto valid_module;
                        }
                    }
                    break;
                }
            }
        }


        //remover um modulo, pois ele nao foi encontrado em nenhuma das listas de validade
        //eh pulado caso o modulo seja valido
        target->modules[i] = NULL;
        target->entropy--;

        valid_module:;
    }




    

    int end_entropy = target->entropy;


    //checa se agora a celula esta entre as de menor entropia, e muda a lista de acordo
    if(end_entropy>1){
        pthread_mutex_lock(&__lock_lowest_entropy);

        if(end_entropy<__lowest_entropy /*|| __num_cells_lowest_entropy==0*/){
            __lowest_entropy=end_entropy;
            clear(__lowest_entropy_cells);
            __num_cells_lowest_entropy=0;
        }
        if(end_entropy==__lowest_entropy){
            if(insertLast_non_repete(__lowest_entropy_cells, target->pos)){
                __num_cells_lowest_entropy++;
            }
        }

        pthread_mutex_unlock(&__lock_lowest_entropy);
    }


    //remove da lista de menor entropia caso a celula tenha sido colapsada por exclusao
    if(end_entropy==1){
        pthread_mutex_lock(&__lock_lowest_entropy);

        struct node* temp = find_by_data(__lowest_entropy_cells, target->pos);
        if(temp!=NULL){
            delete(__lowest_entropy_cells, temp->key);  //pode ter algum erro aqui
            __num_cells_lowest_entropy--;
        }

        pthread_mutex_unlock(&__lock_lowest_entropy);
    }


    //coloca na queue de celulas com entropia alterada, para checar o efeito disso nas adjacentes
    if(end_entropy<target_entropy){
        pthread_mutex_lock(&__lock_occupied);
        enQueue(__affected_cells, target->pos);//lock por aqui mahomenos

        for(int i = 0; i<target_entropy;i++){
            if(target->modules[i] != NULL){
                for(int j =0; j<i;j++){
                    if(target->modules[j]==NULL){
                        target->modules[j] = target->modules[i];
                        target->modules[i] = NULL;
                    }
                }
            }
        }
        pthread_cond_signal(&__cond_propagate);
        pthread_mutex_unlock(&__lock_occupied);
    }
}


//Propaga o efeito do colapso de uma celula
void * propagate(void * arg){
    Arg * args = arg;
    Cell_2d** grid = args->grid;
    int id = args->id;


    Pos_2d origin_pos;

    //controlam os constrains
    bool t=false,b=false,l=false,r=false;
    int completed=0;

    //controla se as celulas adjcentes estao ocupadas
    int free_adjs;

    while(true){
        //printf("Thread %d comecando loop\n", id);
        pthread_mutex_lock(&__lock_occupied);
        //__active_threads--; //usado na barreira

        //checa se a queue esta vazia
        //printf("%p\n", __affected_cells);
        //printf("%p\n", __affected_cells->front);
        while(__affected_cells->front==NULL){
            //printf("No while\n");
            if(__active_threads==0){
                //printf("Signal collapse\n");
                __propagating = false;
                pthread_cond_signal(&__cond_collapse);    //libera os proximos collapsos
            }
            //printf("Thread %d se bloqueando\n", id);
            pthread_cond_wait(&__cond_propagate, &__lock_occupied);
            //printf("Thread %d desbloqueando\n", id);
        }
        //printf("Thread %d comecando\n", id);
        __active_threads++; //usado na barreira


        //Pega a proxima celula da queue
        origin_pos = __affected_cells->front->key;
        deQueue(__affected_cells);

        
        /*Checa se o origin e suas celulas adjacentes nao estao ocupados,
        evitando assim o deadlock termos duas celulas origin adjacentes ao mesmo tempo*/
        free_adjs=0;
        while(free_adjs<4){
            //printf("1\n");
            free_adjs=0;
            if(!grid[origin_pos.lin][origin_pos.col].occupied){
                if(origin_pos.lin>0){
                    if(!grid[origin_pos.lin-1][origin_pos.col].occupied){
                        free_adjs++;
                    }
                }else{
                    free_adjs++;
                }
                if(origin_pos.lin<__size_lin-1){
                    if(!grid[origin_pos.lin+1][origin_pos.col].occupied){
                        free_adjs++;
                    }
                }else{
                    free_adjs++;
                }
                if(origin_pos.col>0){
                    if(!grid[origin_pos.lin][origin_pos.col-1].occupied){
                        free_adjs++;
                    }
                }else{
                    free_adjs++;
                }
                if(origin_pos.col<__size_col-1){
                    if(!grid[origin_pos.lin][origin_pos.col+1].occupied){
                        free_adjs++;
                    }
                }else{
                    free_adjs++;
                }
            }
            if(free_adjs<4){
                pthread_cond_wait(&__cond_occupied, &__lock_occupied);  //entra em wait caso o origin ou suas adjacencias estejam ocupados
            }
            //printf("2\n");
        }
        



        //marca como em uso e tira da queue
        grid[origin_pos.lin][origin_pos.col].occupied=true;   
        
        pthread_mutex_unlock(&__lock_occupied);







        //zera as variaveis
        t=false,b=false,l=false,r=false;
        completed=0;

        /*roda o constrain para cada celula adjacente, 
        ficando em espera ocupada caso todas as celulas que faltam estiverem em operacao*/
        while(completed<4){
            //printf("while loop - completed: %d\n", completed);
            //printf("t:%d - b:%d - l:%d - r:%d - \n", t, b, l, r);

            if(!t){
                if(origin_pos.lin<__size_lin-1){
                    pthread_mutex_lock(&__lock_occupied);
                    //printf("Thread %d checando occupied t\n", id);
                    if(!grid[origin_pos.lin+1][origin_pos.col].occupied){
                        //printf("Thread %d pos not occupied t\n", id);
                        grid[origin_pos.lin+1][origin_pos.col].occupied=true;
                        pthread_mutex_unlock(&__lock_occupied);
            
                        constrain(&(grid[origin_pos.lin][origin_pos.col]), &(grid[origin_pos.lin+1][origin_pos.col]), 't');
                        completed++;
                        t=true;

                        pthread_mutex_lock(&__lock_occupied);
                        grid[origin_pos.lin+1][origin_pos.col].occupied=false;
                        pthread_mutex_unlock(&__lock_occupied);
                    } else{
                        //printf("Thread %d pos occupied t\n", id);
                        pthread_mutex_unlock(&__lock_occupied);
                    }
                } else  {
                    completed++;
                    t=true;
                }
                
            }
            

            if(!b){
                if(origin_pos.lin>0){
                    pthread_mutex_lock(&__lock_occupied);
                    //printf("Thread %d checando occupied b\n", id);
                    if(!grid[origin_pos.lin-1][origin_pos.col].occupied){
                        //printf("Thread %d pos not occupied b\n", id);
                        grid[origin_pos.lin-1][origin_pos.col].occupied=true;
                        pthread_mutex_unlock(&__lock_occupied);
            
                        constrain(&grid[origin_pos.lin][origin_pos.col], &grid[origin_pos.lin-1][origin_pos.col], 'b');
                        completed++;
                        b=true;
                        
                        pthread_mutex_lock(&__lock_occupied);
                        grid[origin_pos.lin-1][origin_pos.col].occupied=false;
                        pthread_mutex_unlock(&__lock_occupied);
                    } else{
                        //printf("Thread %d pos occupied b\n", id);
                        pthread_mutex_unlock(&__lock_occupied);
                    }
                } else{
                    completed++;
                    b=true;
                }
            }

            

            if(!l){
                if(origin_pos.col>0){
                    pthread_mutex_lock(&__lock_occupied);
                    //printf("Thread %d checando occupied l\n", id);
                    if(!grid[origin_pos.lin][origin_pos.col-1].occupied){
                        //printf("Thread %d pos not occupied l\n", id);
                        grid[origin_pos.lin][origin_pos.col-1].occupied=true;
                        pthread_mutex_unlock(&__lock_occupied);
            
                        constrain(&grid[origin_pos.lin][origin_pos.col], &grid[origin_pos.lin][origin_pos.col-1], 'l');
                        completed++;
                        l=true;
                        
                        pthread_mutex_lock(&__lock_occupied);
                        grid[origin_pos.lin][origin_pos.col-1].occupied=false;
                        pthread_mutex_unlock(&__lock_occupied);
                    } else{
                        //printf("Thread %d pos occupied l\n", id);
                        pthread_mutex_unlock(&__lock_occupied);
                    }
                } else {
                    completed++;
                    l=true;
                }
            }

            

            if(!r){
                if(origin_pos.col<__size_col-1){
                    pthread_mutex_lock(&__lock_occupied);
                    //printf("Thread %d checando occupied r\n", id);
                    if(!grid[origin_pos.lin][origin_pos.col+1].occupied){
                        //printf("Thread %d pos not occupied r\n", id);
                        grid[origin_pos.lin][origin_pos.col+1].occupied=true;
                        pthread_mutex_unlock(&__lock_occupied);
            
                        constrain(&grid[origin_pos.lin][origin_pos.col], &grid[origin_pos.lin][origin_pos.col+1], 'r');
                        completed++;
                        r=true;
                        
                        pthread_mutex_lock(&__lock_occupied);
                        grid[origin_pos.lin][origin_pos.col+1].occupied=false;
                        pthread_mutex_unlock(&__lock_occupied);
                    } else{
                        //printf("Thread %d pos occupied r\n", id);
                        pthread_mutex_unlock(&__lock_occupied);
                    }
                } else {
                    completed++;
                    r=true;
                }
            }

        }
        //printf("Thread %d terminou de propagar\n", id);

        //marca a celula origin como livre e libera todas as threads em espera de origin
        pthread_mutex_lock(&__lock_occupied);    
        grid[origin_pos.lin][origin_pos.col].occupied=false;
        pthread_cond_broadcast(&__cond_occupied);
        __active_threads--;
        pthread_mutex_unlock(&__lock_occupied);
        
    }

}







/*
Inicia o collapso de uma celula e chama as funções que calculam todos os efeitos colaterais
*/
void collapse(Cell_2d** grid, Pos_2d pos){

    pthread_mutex_lock(&__lock_occupied);

    Cell_2d* cell = &grid[pos.lin][pos.col];
    

    //nao pode chamar collapse em celulas com entropia menor que 2
    if(cell->entropy <= 1) {  printf("Error: Tried to collapse cell with entropy of 1 or less\n"); exit(1);   }

    

    int ceiling = cell->entropy-1;
    int guess = choose_rand(ceiling);


    //torna nulos todas as referencias a modulos que não foram a escolhida e traz a nao nula para frente
    collapse_cell(cell, guess);


    
    //preparando para propagar
    enQueue(__affected_cells, pos);
    pthread_cond_signal(&__cond_propagate);
    __propagating=true;

    //barreira esperando o fim da propagacao
    while(__propagating){
        pthread_cond_wait(&__cond_collapse, &__lock_occupied);
    }
    printf("active threads: %d\n", __active_threads);
    pthread_mutex_unlock(&__lock_occupied);
    
}


Cell_2d** setup_test(){
    Cell_2d** grid;


    __num_modules = NUM_MODULES;
    srand ( time(NULL) );
    
    __lowest_entropy=__num_modules;
    __num_cells_lowest_entropy = 0;

    __size_lin = 10;
    __size_col = 15;

    __affected_cells = createQueue();
    

    printf("Aloca modulos\n");
    Module_2d* modulos = (Module_2d*)malloc(sizeof(Module_2d)*__num_modules);
    for(int i = 0; i<__num_modules; i++){
        modulos[i].neigBotton = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigTop = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigLeft = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigRight = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        for(int j=0; j<__num_modules; j++){
            modulos[i].neigBotton[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigTop[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigLeft[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigRight[j] = (Module_2d*)malloc(sizeof(Module_2d));
        }
    }


    char* A = "▓";
    char* B = "█";
    char* C = "░";
    char* D = " ";
    modulos[0].color.a =   A;
    modulos[1].color.a =   B;
    modulos[2].color.a =   C;
    modulos[3].color.a =   D;
    
    
    //regras
    printf("Seta regras\n");
    modulos[0].neigBotton[0]=&modulos[0];
    modulos[0].sizeNeigBotton=1;

    modulos[0].neigTop[0]=&modulos[0];
    modulos[0].neigTop[1]=&modulos[1];
    modulos[0].neigTop[2]=&modulos[3];
    modulos[0].sizeNeigTop=3;

    modulos[0].neigLeft[0]=&modulos[0];
    modulos[0].neigLeft[1]=&modulos[3];
    modulos[0].sizeNeigLeft=2;

    modulos[0].neigRight[0]=&modulos[0];
    modulos[0].neigRight[1]=&modulos[3];
    modulos[0].sizeNeigRight=2;



    modulos[1].neigBotton[0]=&modulos[0];
    modulos[1].neigBotton[1]=&modulos[1];
    modulos[1].sizeNeigBotton=2;

    modulos[1].neigTop[0]=&modulos[1];
    modulos[1].neigTop[1]=&modulos[2];
    modulos[1].sizeNeigTop=2;

    modulos[1].neigLeft[0]=&modulos[3];
    modulos[1].sizeNeigLeft=1;

    modulos[1].neigRight[0]=&modulos[3];
    modulos[1].sizeNeigRight=1;




    modulos[2].neigBotton[0]=&modulos[1];
    modulos[2].neigBotton[1]=&modulos[2];
    modulos[2].neigBotton[2]=&modulos[3];
    modulos[2].sizeNeigBotton=3;

    modulos[2].neigTop[0]=&modulos[2];
    modulos[2].neigTop[1]=&modulos[3];
    modulos[2].sizeNeigTop=2;

    modulos[2].neigLeft[0]=&modulos[2];
    modulos[2].neigLeft[1]=&modulos[3];
    modulos[2].sizeNeigLeft=2;

    modulos[2].neigRight[0]=&modulos[2];
    modulos[2].neigRight[1]=&modulos[3];
    modulos[2].sizeNeigRight=2;





    modulos[3].neigBotton[0]=&modulos[0];
    modulos[3].neigBotton[1]=&modulos[2];
    modulos[3].neigBotton[2]=&modulos[3];
    modulos[3].sizeNeigBotton=3;

    modulos[3].neigTop[0]=&modulos[2];
    modulos[3].neigTop[1]=&modulos[3];
    modulos[3].sizeNeigTop=2;

    modulos[3].neigLeft[0]=&modulos[0];
    modulos[3].neigLeft[1]=&modulos[1];
    modulos[3].neigLeft[2]=&modulos[2];
    modulos[3].neigLeft[3]=&modulos[3];
    modulos[3].sizeNeigLeft=__num_modules;

    modulos[3].neigRight[0]=&modulos[0];
    modulos[3].neigRight[1]=&modulos[1];
    modulos[3].neigRight[2]=&modulos[2];
    modulos[3].neigRight[3]=&modulos[3];
    modulos[3].sizeNeigRight=__num_modules;


    printf("\n");
    for(int i = 0;i < __num_modules; i++){
        printf("Modulo %d\n", i);

        printf("size B: %d\n", modulos[i].sizeNeigBotton);
        for(int j = 0; j< modulos[i].sizeNeigBotton; j++){
            printf("%s|", modulos[i].neigBotton[j]->color.a);
        }
        printf("\n");

        printf("size T: %d\n", modulos[i].sizeNeigTop);
        for(int j = 0; j< modulos[i].sizeNeigTop; j++){
            printf("%s|", modulos[i].neigTop[j]->color.a);
        }
        printf("\n");

        printf("size L: %d\n", modulos[i].sizeNeigLeft);
        for(int j = 0; j< modulos[i].sizeNeigLeft; j++){
            printf("%s|", modulos[i].neigLeft[j]->color.a);
        }
        printf("\n");

        printf("size R: %d\n", modulos[i].sizeNeigRight);
        for(int j = 0; j< modulos[i].sizeNeigRight; j++){
            printf("%s|", modulos[i].neigRight[j]->color.a);
        }
        printf("\n");

        printf("\n");
    }




    //aloca grid
    printf("Aloca grid\n");
    grid = (Cell_2d**)malloc(sizeof(Cell_2d*)*__size_lin);
    for(int i =0; i<__size_lin; i++){
        grid[i]=(Cell_2d*)malloc(sizeof(Cell_2d)*__size_col);
        for(int j =0; j<__size_col; j++){
            grid[i][j].modules = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
            grid[i][j].modules[0] = (Module_2d*)malloc(sizeof(Module_2d));
            grid[i][j].modules[1] = (Module_2d*)malloc(sizeof(Module_2d));
            grid[i][j].modules[2] = (Module_2d*)malloc(sizeof(Module_2d));
            grid[i][j].modules[3] = (Module_2d*)malloc(sizeof(Module_2d));
        }
    }

    //preenche grid
    printf("Preenche grid\n");
    for(int i = 0; i<__size_lin; i++){
        for(int j = 0; j<__size_col; j++){
            grid[i][j].pos.lin = i; 
            grid[i][j].pos.col = j;
            grid[i][j].entropy = __num_modules;
            grid[i][j].occupied = false;


            grid[i][j].modules[0] = &(modulos[0]);
            grid[i][j].modules[1] = &(modulos[1]);
            grid[i][j].modules[2] = &(modulos[2]);
            grid[i][j].modules[3] = &(modulos[3]);
        }
    }


    printf("Retorna grid\n");
    return grid;
}



/*
Recebe o endereco do arquivo de entrada, o le, registra o numero de modulos e suas regras, e inicializa a grid.
*/
Cell_2d** setup(char* adress){
    //seed do rand()
    srand ( time(NULL) );

    Cell_2d** grid;
    __affected_cells = createQueue(); //queue de celulas afetadas no collapse e sua propagacao

    __num_cells_lowest_entropy = 0; //numero de celulas de menor entropia

    //input() funcao que le o input e pega as informações necessarias


    //__num_modules = numero de modulos lido;   //numero de modulos
    //__lowest_entropy=__num_modules;   //menor entropia inicial = numero de modulos
    
    //__size_lin = numero de linha pedido;  //numero de linha pedido
    //__size_col = numero de colunas pedido;    //umero de colunas pedido

    //allocate_modules();
    //set_rules();
    //grid = allocate_grid(infos necessarias);
    //preenche_grid(grid);

    return grid;
}



/*
Wave Function Collapse.
Recebe o endereço de um arquivo de entrada, contendo os modulos possiveis, as regras e o tamanho desejado da grid.
*/
void wfc(char* adress){

    //seed do rand()
    //srand ( time(NULL) );

    __active_threads=0;

    __NTHREADS=4;

    pthread_t tid[__NTHREADS];


    pthread_mutex_init(&__lock_occupied, NULL);
    pthread_mutex_init(&__lock_lowest_entropy, NULL);
    pthread_cond_init(&__cond_propagate, NULL);
    pthread_cond_init(&__cond_occupied, NULL);
    pthread_cond_init(&__cond_collapse, NULL);

    
    
    __lowest_entropy=__num_modules;
    __num_cells_lowest_entropy = 0;



    //__affected_cells = createQueue();
    __lowest_entropy_cells = (struct node**)malloc(sizeof(struct node*));
    __lowest_entropy_cells[0] = NULL;


    //Cria e preenche a grid
    Cell_2d** grid;
    //grid = setup(adress);
    grid = setup_test(); //teste base

    Arg arg;
    arg.grid = grid;
    

    //cria as threads
    for(int i=0; i<__NTHREADS; i++) {
        arg.id = i;
        if(pthread_create(&tid[i], NULL, propagate, (void *) &arg)) exit(-1);
    } 

    //posicao randomica na lista de celulas de menor entropia
    int rand_cell=0;

    //o node retirado da lista de celulas de menor entropia
    struct node* next_node;

    //proxima posicao a ser collapsada
    Pos_2d next;
    next.lin = choose_rand(__size_lin-1);
    next.col = choose_rand(__size_col-1);

    //primeira inserção na lista
    insertLast_non_repete(__lowest_entropy_cells, next);
    __num_cells_lowest_entropy=1;


    //roda enquanto houverem celulas nao colapsadas
    while(!isEmpty(__lowest_entropy_cells)){
        //deletando a proxima celula a ser collapsada da lista
        delete(__lowest_entropy_cells, rand_cell);
        __num_cells_lowest_entropy--;


        //collapsando celula
        printf("Colapsando (%d,%d)\n", next.lin+1, next.col+1);
        collapse(grid, next);
        printf("ncle: %d\n", __num_cells_lowest_entropy);


        //se a lista de celulas de menor entropia esta vazia, faz uma varredura da grid por celulas nao collapsadas
        if(__num_cells_lowest_entropy == 0){
            __lowest_entropy = __num_modules;
            backtrack(grid);
        }


        //proxima celula a ser collapsada
        rand_cell = choose_rand(__num_cells_lowest_entropy-1);

       
        //node da proxima celula a ser collapsada
        next_node = find(__lowest_entropy_cells, rand_cell); 
        if(next_node==NULL){
            break;
        }


        //atualizando next
        next = next_node->data;


        print_grid(grid);
    }
    print_grid(grid);
    printf("Fim!\n");
}





int main(void){
    wfc("");
}
/*
remove da celula target qualquer modulo que nao seja permitido pela celula root
*/
void constrain(Cell_2d* root, Cell_2d* target, char dir){
    int target_entropy = target->entropy;
    int root_entropy = root->entropy;
    int size_neig;


    /*
    loopa pelos modulos de target, checando se sao validos,
    loopando pela lista de modulos validos do lado correspondente de cada
    modulo possivel de root
    */
    for(int i =0; i < target_entropy; i++){

        for(int j =0; j < root_entropy; j++){

            switch (dir) {
                //casa com a direcao recebida
                case 't' :{
                    size_neig = root[0].modules[j]->sizeNeigTop;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == root->modules[j]->neigTop[k]){
                            goto valid_module;
                        }
                    }
                    break;
                }
                case 'b' :{
                    size_neig= root->modules[j]->sizeNeigBotton;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == root->modules[j]->neigBotton[k]){
                            goto valid_module;
                        }
                    }
                    break;
                }
                case 'r' :{
                    size_neig= root->modules[j]->sizeNeigRight;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == root->modules[j]->neigRight[k]){
                            goto valid_module;
                        }
                    }
                    break;
                }
                case 'l' :{
                    size_neig= root->modules[j]->sizeNeigLeft;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == root->modules[j]->neigLeft[k]){
                            goto valid_module;
                        }
                    }
                    break;
                }
            }
        }


        //remover um modulo, pois ele nao foi encontrado em nenhuma das listas de validade
        //eh pulado caso o modulo seja valido
        target->modules[i] = NULL;
        target->entropy--;

        valid_module:;
    }




    

    int end_entropy = target->entropy;


    //checa se agora a celula esta entre as de menor entropia, e muda a lista de acordo
    if(end_entropy>1){
        if(end_entropy<__lowest_entropy /*|| __num_cells_lowest_entropy==0*/){
            __lowest_entropy=end_entropy;
            clear(__lowest_entropy_cells);
            __num_cells_lowest_entropy=0;
        }
        if(end_entropy==__lowest_entropy){
            if(insertLast_non_repete(__lowest_entropy_cells, target->pos)){
                __num_cells_lowest_entropy++;
            }
        }
    }


    //remove da lista de menor entropia caso a celula tenha sido colapsada por exclusao
    if(end_entropy==1){
        struct node* temp = find_by_data(__lowest_entropy_cells, target->pos);
        if(temp!=NULL){
            delete(__lowest_entropy_cells, temp->key);  //pode ter algum erro aqui
            __num_cells_lowest_entropy--;
        }
    }


    //coloca na queue de celulas com entropia alterada, para checar o efeito disso nas adjacentes
    if(end_entropy<target_entropy){
        enQueue(__affected_cells, target->pos);

        for(int i = 0; i<target_entropy;i++){
            if(target->modules[i] != NULL){
                for(int j =0; j<i;j++){
                    if(target->modules[j]==NULL){
                        target->modules[j] = target->modules[i];
                        target->modules[i] = NULL;
                    }
                }
            }
        }
    }
}
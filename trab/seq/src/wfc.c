#include "../libs/include.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



//numero de modulos para o teste
#define NUM_MODULES 4

/*
temos uma grid de celulas, cada uma possuindo uma lista de 
modulos validos para essa posicao.
Cada modulo tem sua cor e lista de modulos adjacentes validos
*/




//Variaveis globais
int __num_cells_lowest_entropy;
int __lowest_entropy;   //menor entropia global

int __size_lin;
int __size_col;

int __num_modules;

char __print;

struct Queue* __affected_cells;  
struct node** __lowest_entropy_cells = NULL;




/*
Printa a grid para o terminal
*/
void print_grid(Cell_2d** grid){
    if(__print == 's'){
        //borda superior
        for(int i =0; i< __size_col*2+2; i++) printf("-");
        printf("\n");
        

        for(int i =__size_lin-1; i>=0; i--){
            printf("|");//borda esquerda

            for(int j =0; j< __size_col; j++){
                grid[i][j].entropy != 1? printf("%2d", grid[i][j].entropy): printf("%s%s", grid[i][j].modules[0]->color.a, grid[i][j].modules[0]->color.a); 
            }

            printf("|");//borda direita
            printf("\n");
        }

        //borda inferior
        for(int i =0; i< __size_col*2+2; i++) printf("-");
        printf("\n");
        printf("\n");
    }
    
}



void is_correct(Cell_2d** grid){

    int size_neig=0;

    for(int i =0; i < __size_lin; i++){

        for(int j =0; j < __size_col; j++){
            
            if(i<__size_lin-1){
                size_neig = grid[i][j].modules[0]->sizeNeigTop;
                for(int k=0;k<size_neig;k++){
                    if(grid[i+1][j].modules[0] == grid[i][j].modules[0]->neigTop[k]){
                        goto check_bottom;
                    }
                }
                printf("ERRO: Regra Ferida\n");
                exit(1);
            }


            check_bottom:;
            if(i>0){
                size_neig = grid[i][j].modules[0]->sizeNeigBotton;
                for(int k=0;k<size_neig;k++){
                    if(grid[i-1][j].modules[0] == grid[i][j].modules[0]->neigBotton[k]){
                        goto check_left;
                    }
                }
                printf("ERRO: Regra Ferida\n");
                exit(1);
            }


            check_left:;
            if(j>0){
                size_neig = grid[i][j].modules[0]->sizeNeigLeft;
                for(int k=0;k<size_neig;k++){
                    if(grid[i][j-1].modules[0] == grid[i][j].modules[0]->neigLeft[k]){
                        goto check_right;
                    }
                }
                printf("ERRO: Regra Ferida\n");
                exit(1);
            }


            check_right:;
            if(j<__size_col-1){
                size_neig = grid[i][j].modules[0]->sizeNeigRight;
                for(int k=0;k<size_neig;k++){
                    if(grid[i][j+1].modules[0] == grid[i][j].modules[0]->neigRight[k]){
                        goto valid_modules;
                    }
                }
                printf("ERRO: Regra Ferida\n");
                exit(1);
            }


            valid_modules:;





        }
    }

    printf("Mapa correto\n");
}



/*
Percorre a grid, adicionando as celulas com menor entropia na lista delas
*/
void backtrack(Cell_2d** grid){
    
    if(__print == 's'){
        printf("backtrack\n");
        printf("ncle pre backtrack: %d\n", __num_cells_lowest_entropy);
        print_grid(grid);
    }
    
    

    /*
    compara a entropia de cada celula que percorre com a menor_entropia(__lowest_entropy)
    caso menor, a substitui e limpa a lista de menores_entropias, 
    caso igual, adiciona a celula a lista
    */
    int entropy=__num_modules;
    for(int i =0; i<__size_lin; i++){
        for(int j =0; j<__size_col; j++){

            entropy = grid[i][j].entropy;

            if(entropy>1){
                if(entropy<__lowest_entropy){
                    clear(__lowest_entropy_cells);
                    __num_cells_lowest_entropy = 0;
                    __lowest_entropy = entropy;
                }
                if(entropy==__lowest_entropy){
                    insertLast(__lowest_entropy_cells, grid[i][j].pos);
                    __num_cells_lowest_entropy++;
                }
            }
        }
    }
    if(__print == 's'){
        printf("ncle pos backtrack: %d\n", __num_cells_lowest_entropy);
    }
}






/*
Retorna um int entre 0 e ceiling incluso
*/
int choose_rand(int ceiling){
    return (int)(((float)rand()/(float)RAND_MAX)*(((float)ceiling)+0.99999)); //escolhe de [0..ceiling]
}


/*
Colapsa uma celula e reordena seu modulo restante
*/
void collapse_cell(Cell_2d* cell, int guess){
    int ceiling = cell->entropy;

    for(int i=0; i<ceiling; i++){
        if(i!=guess){
            cell->modules[i] = NULL;
        }
    }

    cell->modules[0]=cell->modules[guess];
    if(guess!=0){cell->modules[guess]=NULL;}
    cell->entropy=1;
}




/*
remove da celula target qualquer modulo que nao seja permitido pela celula root
*/
void constrain(Cell_2d* root, Cell_2d* target, char dir){
    int target_entropy = target->entropy;
    int root_entropy = root->entropy;
    int size_neig;


    /*
    loopa pelos modulos de target, checando se sao validos,
    loopando pela lista de modulos validos do lado correspondente de cada
    modulo possivel de root
    */
    for(int i =0; i < target_entropy; i++){

        for(int j =0; j < root_entropy; j++){

            switch (dir) {
                //casa com a direcao recebida
                case 't' :{
                    size_neig = root[0].modules[j]->sizeNeigTop;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == root->modules[j]->neigTop[k]){
                            goto valid_module;
                        }
                    }
                    break;
                }
                case 'b' :{
                    size_neig= root->modules[j]->sizeNeigBotton;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == root->modules[j]->neigBotton[k]){
                            goto valid_module;
                        }
                    }
                    break;
                }
                case 'r' :{
                    size_neig= root->modules[j]->sizeNeigRight;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == root->modules[j]->neigRight[k]){
                            goto valid_module;
                        }
                    }
                    break;
                }
                case 'l' :{
                    size_neig= root->modules[j]->sizeNeigLeft;

                    for(int k=0;k<size_neig;k++){
                        if(target->modules[i] == root->modules[j]->neigLeft[k]){
                            goto valid_module;
                        }
                    }
                    break;
                }
            }
        }


        //remover um modulo, pois ele nao foi encontrado em nenhuma das listas de validade
        //eh pulado caso o modulo seja valido
        target->modules[i] = NULL;
        target->entropy--;

        valid_module:;
    }




    

    int end_entropy = target->entropy;


    //checa se agora a celula esta entre as de menor entropia, e muda a lista de acordo
    if(end_entropy>1){
        if(end_entropy<__lowest_entropy /*|| __num_cells_lowest_entropy==0*/){
            __lowest_entropy=end_entropy;
            clear(__lowest_entropy_cells);
            __num_cells_lowest_entropy=0;
        }
        if(end_entropy==__lowest_entropy){
            if(insertLast_non_repete(__lowest_entropy_cells, target->pos)){
                __num_cells_lowest_entropy++;
            }
        }
    }


    //remove da lista de menor entropia caso a celula tenha sido colapsada por exclusao
    if(end_entropy==1){
        struct node* temp = find_by_data(__lowest_entropy_cells, target->pos);
        if(temp!=NULL){
            delete(__lowest_entropy_cells, temp->key);  //pode ter algum erro aqui
            __num_cells_lowest_entropy--;
        }
    }


    //coloca na queue de celulas com entropia alterada, para checar o efeito disso nas adjacentes
    if(end_entropy<target_entropy){
        enQueue(__affected_cells, target->pos);

        for(int i = 0; i<target_entropy;i++){
            if(target->modules[i] != NULL){
                for(int j =0; j<i;j++){
                    if(target->modules[j]==NULL){
                        target->modules[j] = target->modules[i];
                        target->modules[i] = NULL;
                    }
                }
            }
        }
    }
}


/*
Propaga o efeito de um collapse sobre as celulas adjacentes
*/
void propagate(Cell_2d** grid, Pos_2d root_pos){
    
    Pos_2d target_pos;

    //adiciona a queue de celula com entropia alterada, que pode ser aumentada pelo constrain
    enQueue(__affected_cells, root_pos);

    while(__affected_cells->front!=NULL){
        target_pos = __affected_cells->front->key;
        deQueue(__affected_cells);


        //roda o constrain para cada celula adjacente
        if(target_pos.lin<__size_lin-1){
            constrain(&(grid[target_pos.lin][target_pos.col]), &(grid[target_pos.lin+1][target_pos.col]), 't');
        }
        if(target_pos.lin>0){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin-1][target_pos.col], 'b');
        }
        if(target_pos.col>0){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin][target_pos.col-1], 'l');
        }
        if(target_pos.col<__size_col-1){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin][target_pos.col+1], 'r');
        }

    }

}



/*
Inicia o collapso de uma celula e chama as funções que calculam todos os efeitos colaterais
*/
void collapse(Cell_2d** grid, Pos_2d pos){

    Cell_2d* cell = &grid[pos.lin][pos.col];
    

    //nao pode chamar collapse em celulas com entropia menor que 2
    if(cell->entropy <= 1) {  printf("Error: Tried to collapse cell with entropy of 1 or less\n"); exit(1);   }

    

    int ceiling = cell->entropy-1;
    int guess = choose_rand(ceiling);


    //torna nulos todas as referencias a modulos que não foram a escolhida e traz a nao nula para frente
    collapse_cell(cell, guess);


    propagate(grid, cell->pos);
}


Cell_2d** setup_test(){
    Cell_2d** grid;


    __num_modules = NUM_MODULES;
    srand ( time(NULL) );
    
    __lowest_entropy=__num_modules;
    __num_cells_lowest_entropy = 0;

    __size_lin = 10;
    __size_col = 15;

    __affected_cells = createQueue();
    

    printf("Aloca modulos\n");
    Module_2d* modulos = (Module_2d*)malloc(sizeof(Module_2d)*__num_modules);
    for(int i = 0; i<__num_modules; i++){
        modulos[i].neigBotton = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigTop = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigLeft = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigRight = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        for(int j=0; j<__num_modules; j++){
            modulos[i].neigBotton[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigTop[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigLeft[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigRight[j] = (Module_2d*)malloc(sizeof(Module_2d));
        }
    }


    char* A = "▓";
    char* B = "█";
    char* C = "░";
    char* D = " ";
    modulos[0].color.a =   A;
    modulos[1].color.a =   B;
    modulos[2].color.a =   C;
    modulos[3].color.a =   D;
    
    
    //regras
    printf("Seta regras\n");
    modulos[0].neigBotton[0]=&modulos[0];
    modulos[0].sizeNeigBotton=1;

    modulos[0].neigTop[0]=&modulos[0];
    modulos[0].neigTop[1]=&modulos[1];
    modulos[0].neigTop[2]=&modulos[3];
    modulos[0].sizeNeigTop=3;

    modulos[0].neigLeft[0]=&modulos[0];
    modulos[0].neigLeft[1]=&modulos[3];
    modulos[0].sizeNeigLeft=2;

    modulos[0].neigRight[0]=&modulos[0];
    modulos[0].neigRight[1]=&modulos[3];
    modulos[0].sizeNeigRight=2;



    modulos[1].neigBotton[0]=&modulos[0];
    modulos[1].neigBotton[1]=&modulos[1];
    modulos[1].sizeNeigBotton=2;

    modulos[1].neigTop[0]=&modulos[1];
    modulos[1].neigTop[1]=&modulos[2];
    modulos[1].sizeNeigTop=2;

    modulos[1].neigLeft[0]=&modulos[3];
    modulos[1].sizeNeigLeft=1;

    modulos[1].neigRight[0]=&modulos[3];
    modulos[1].sizeNeigRight=1;




    modulos[2].neigBotton[0]=&modulos[1];
    modulos[2].neigBotton[1]=&modulos[2];
    modulos[2].neigBotton[2]=&modulos[3];
    modulos[2].sizeNeigBotton=3;

    modulos[2].neigTop[0]=&modulos[2];
    modulos[2].neigTop[1]=&modulos[3];
    modulos[2].sizeNeigTop=2;

    modulos[2].neigLeft[0]=&modulos[2];
    modulos[2].neigLeft[1]=&modulos[3];
    modulos[2].sizeNeigLeft=2;

    modulos[2].neigRight[0]=&modulos[2];
    modulos[2].neigRight[1]=&modulos[3];
    modulos[2].sizeNeigRight=2;





    modulos[3].neigBotton[0]=&modulos[0];
    modulos[3].neigBotton[1]=&modulos[2];
    modulos[3].neigBotton[2]=&modulos[3];
    modulos[3].sizeNeigBotton=3;

    modulos[3].neigTop[0]=&modulos[2];
    modulos[3].neigTop[1]=&modulos[3];
    modulos[3].sizeNeigTop=2;

    modulos[3].neigLeft[0]=&modulos[0];
    modulos[3].neigLeft[1]=&modulos[1];
    modulos[3].neigLeft[2]=&modulos[2];
    modulos[3].neigLeft[3]=&modulos[3];
    modulos[3].sizeNeigLeft=__num_modules;

    modulos[3].neigRight[0]=&modulos[0];
    modulos[3].neigRight[1]=&modulos[1];
    modulos[3].neigRight[2]=&modulos[2];
    modulos[3].neigRight[3]=&modulos[3];
    modulos[3].sizeNeigRight=__num_modules;


    printf("\n");
    for(int i = 0;i < __num_modules; i++){
        printf("Modulo %d\n", i);

        printf("size B: %d\n", modulos[i].sizeNeigBotton);
        for(int j = 0; j< modulos[i].sizeNeigBotton; j++){
            printf("%s|", modulos[i].neigBotton[j]->color.a);
        }
        printf("\n");

        printf("size T: %d\n", modulos[i].sizeNeigTop);
        for(int j = 0; j< modulos[i].sizeNeigTop; j++){
            printf("%s|", modulos[i].neigTop[j]->color.a);
        }
        printf("\n");

        printf("size L: %d\n", modulos[i].sizeNeigLeft);
        for(int j = 0; j< modulos[i].sizeNeigLeft; j++){
            printf("%s|", modulos[i].neigLeft[j]->color.a);
        }
        printf("\n");

        printf("size R: %d\n", modulos[i].sizeNeigRight);
        for(int j = 0; j< modulos[i].sizeNeigRight; j++){
            printf("%s|", modulos[i].neigRight[j]->color.a);
        }
        printf("\n");

        printf("\n");
    }




    //aloca grid
    printf("Aloca grid\n");
    grid = (Cell_2d**)malloc(sizeof(Cell_2d*)*__size_lin);
    for(int i =0; i<__size_lin; i++){
        grid[i]=(Cell_2d*)malloc(sizeof(Cell_2d)*__size_col);
        for(int j =0; j<__size_col; j++){
            grid[i][j].modules = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
            grid[i][j].modules[0] = (Module_2d*)malloc(sizeof(Module_2d));
            grid[i][j].modules[1] = (Module_2d*)malloc(sizeof(Module_2d));
            grid[i][j].modules[2] = (Module_2d*)malloc(sizeof(Module_2d));
            grid[i][j].modules[3] = (Module_2d*)malloc(sizeof(Module_2d));
        }
    }

    //preenche grid
    printf("Preenche grid\n");
    for(int i = 0; i<__size_lin; i++){
        for(int j = 0; j<__size_col; j++){
            grid[i][j].pos.lin = i; 
            grid[i][j].pos.col = j;
            grid[i][j].entropy = __num_modules;
            

            grid[i][j].modules[0] = &(modulos[0]);
            grid[i][j].modules[1] = &(modulos[1]);
            grid[i][j].modules[2] = &(modulos[2]);
            grid[i][j].modules[3] = &(modulos[3]);
        }
    }



    return grid;
}

char* parse(char* string){

    int size = ((int)strlen(string))-2;
    
    char* buffer = (char*)malloc(sizeof(char)*(size+1));

    for(int i = 0; i<size; i++){
        buffer[i] = string[i+1];
    }

    buffer[size]='\0';

    return buffer;
    

}

/*
Recebe o endereco do arquivo de entrada, o le, registra o numero de modulos e suas regras, e inicializa a grid.
*/
Cell_2d** setup(char* adress){

    FILE *fp;              
	char buffer[20480];
	struct json_object *parsed_json;    //blibioteca json-c.lib que faz parser(pesquisar) do arquivo texto em objeto
	int i, j, k, sizeNeig;    
    json_object *listaModulosObj, *moduloObj, *valorObj, *valorListaObj, *listaAtributosObj; //objetos de navegação no json
  
    Cell_2d** grid;

    //seed do rand()
    srand ( time(NULL) );
        
    __affected_cells = createQueue();
    
    __num_cells_lowest_entropy = 0;

    // Lê arquivo texto setup.json
	fp = fopen(adress, "r");
	fread(buffer, 20480, 1, fp);
	fclose(fp);

    // Transforma o arquivo texto .json em Objeto
	parsed_json = json_tokener_parse(buffer);

    // Procura no Objeto Json o atributo "grid" que contém o número de linhas e colunas
    if (json_object_object_get_ex(parsed_json, "grid", &listaAtributosObj) == false) {
        printf( "Query Modulos ERRO\n" );
        return NULL;
    }

    // Obtém size-lin x size-col
    if (json_object_object_get_ex(listaAtributosObj, "size-lin", &valorObj )) {
        __size_lin = atoi(json_object_to_json_string( valorObj)); //converte o valor do objeto encontrado em string para depois converter para inteiro
        printf("LINHAS==>%d\n", __size_lin);
    }
    if (json_object_object_get_ex(listaAtributosObj, "size-col", &valorObj )) {
        __size_col = atoi(json_object_to_json_string( valorObj)); //converte o valor do objeto encontrado em string para depois converter para inteiro
        printf("COLUNAS==>%d\n", __size_col);
    }

//-------------------------

    // Procura no Objeto Json o atributo "modulos" que define as regras
    if (json_object_object_get_ex(parsed_json, "modulos", &listaModulosObj) == false) {
        printf( "Query Modulos ERRO\n" );
        return NULL;
    }

    __num_modules = json_object_array_length( listaModulosObj ); //numero de modulos
    __lowest_entropy=__num_modules;   //menor entropia inicial = numero de modulos


    //alocação dos módulos
    printf("Aloca %d modulos \n", __num_modules);
    Module_2d* modulos = (Module_2d*)malloc(sizeof(Module_2d)*__num_modules);
    for(int i = 0; i<__num_modules; i++){
        modulos[i].neigBotton = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigTop = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigLeft = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        modulos[i].neigRight = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);
        for(int j=0; j<__num_modules; j++){
            modulos[i].neigBotton[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigTop[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigLeft[j] = (Module_2d*)malloc(sizeof(Module_2d));
            modulos[i].neigRight[j] = (Module_2d*)malloc(sizeof(Module_2d));
        }
    }

    /* LOOP em cada Módulo para atribuir as regras */
    for ( i = 0; i < __num_modules; i++ ) {

        moduloObj = json_object_array_get_idx( listaModulosObj, i ); //funcao para andar dentro dos arrays que representam as regras

        //inicialização dos números de elementos dos arrays que representam as regras
        modulos[i].sizeNeigBotton = 0;
        modulos[i].sizeNeigTop = 0;
        modulos[i].sizeNeigLeft = 0;
        modulos[i].sizeNeigRight = 0;

        // COR
        if (json_object_object_get_ex( moduloObj, "cor", &valorObj )) {
            modulos[i].color.a = parse(((char *) json_object_to_json_string( valorObj ))); //converte o valor do objeto encontrado em string para 
        }                                                                         //depois converter para char pointer
        
        // neigBotton        
        if (json_object_object_get_ex( moduloObj, "neigBotton", &valorListaObj )) { //obtem o array de índices dos módulos do botton

            sizeNeig = json_object_array_length( valorListaObj );
            modulos[i].sizeNeigBotton = sizeNeig;

            for ( j = 0; j < sizeNeig; j++ ) {
                valorObj = json_object_array_get_idx (valorListaObj, j);
                modulos[i].neigBotton[j] = &modulos[atoi(json_object_to_json_string( valorObj ))]; //atribui a regra
            }
        }

        // neigTop        
        if (json_object_object_get_ex( moduloObj, "neigTop", &valorListaObj )) { //obtem o array de índices dos módulos do top
            
            sizeNeig = json_object_array_length( valorListaObj );
            modulos[i].sizeNeigTop = sizeNeig;

            for ( j = 0; j < sizeNeig; j++ ) {
                valorObj = json_object_array_get_idx (valorListaObj, j);
                modulos[i].neigTop[j] = &modulos[atoi(json_object_to_json_string( valorObj ))]; //atribui a regra
            }
        }

        // neigLeft
        if (json_object_object_get_ex( moduloObj, "neigLeft", &valorListaObj )) { //obtem o array de índices dos módulos do left
            
            sizeNeig = json_object_array_length( valorListaObj );
            modulos[i].sizeNeigLeft = sizeNeig;

            for ( j = 0; j < sizeNeig; j++ ) {
                valorObj = json_object_array_get_idx (valorListaObj, j);
                modulos[i].neigLeft[j] = &modulos[atoi(json_object_to_json_string( valorObj ))]; //atribui a regra
            }
        }

        // neigRight
        if (json_object_object_get_ex( moduloObj, "neigRight", &valorListaObj )) { //obtem o array de índices dos módulos do right
            
            sizeNeig = json_object_array_length( valorListaObj );
            modulos[i].sizeNeigRight = sizeNeig;

            for ( j = 0; j < sizeNeig; j++ ) {
                valorObj = json_object_array_get_idx (valorListaObj, j);
                modulos[i].neigRight[j] = &modulos[atoi(json_object_to_json_string( valorObj ))]; //atribui a regra
            }
        }

    }

    // TESTE
    if(__print == 's'){
        printf("\n");
        for(int i = 0;i < __num_modules; i++){
            printf("Modulo %d\n", i);

            printf("size B: %d\n", modulos[i].sizeNeigBotton);
            for(int j = 0; j< modulos[i].sizeNeigBotton; j++){
                printf("%s|", modulos[i].neigBotton[j]->color.a);
            }
            printf("\n");

            printf("size T: %d\n", modulos[i].sizeNeigTop);
            for(int j = 0; j< modulos[i].sizeNeigTop; j++){
                printf("%s|", modulos[i].neigTop[j]->color.a);
            }
            printf("\n");

            printf("size L: %d\n", modulos[i].sizeNeigLeft);
            for(int j = 0; j< modulos[i].sizeNeigLeft; j++){
                printf("%s|", modulos[i].neigLeft[j]->color.a);
            }
            printf("\n");

            printf("size R: %d\n", modulos[i].sizeNeigRight);
            for(int j = 0; j< modulos[i].sizeNeigRight; j++){
                printf("%s|", modulos[i].neigRight[j]->color.a);
            }
            printf("\n");

            printf("\n");
        }
    }

    // Aloca Grid

    printf("Aloca grid\n");

    grid = (Cell_2d**)malloc(sizeof(Cell_2d*)*__size_lin);

    for(int i =0; i<__size_lin; i++){

        grid[i]=(Cell_2d*)malloc(sizeof(Cell_2d)*__size_col);

        for(int j =0; j<__size_col; j++){
            
            grid[i][j].modules = (Module_2d**)malloc(sizeof(Module_2d*)*__num_modules);

            for(int k=0; k<__num_modules; k++) {
                grid[i][j].modules[k] = (Module_2d*)malloc(sizeof(Module_2d));
            }
        }
    }

    // Preenche Grid
    printf("Preenche grid\n");

    for(int i = 0; i<__size_lin; i++){

        for(int j = 0; j<__size_col; j++){
            
            grid[i][j].pos.lin = i; 
            grid[i][j].pos.col = j;
            grid[i][j].entropy = __num_modules;
            
            for(int k=0; k<__num_modules; k++) {
                grid[i][j].modules[k] = &(modulos[k]);                

            }
        }
    }


    free(listaModulosObj);
    free(moduloObj);
    free(valorObj);
    free(valorListaObj);
    free(listaAtributosObj);
    free(parsed_json);
    //free(fp);   //da erro


    return grid;
}



/*
Wave Function Collapse.
Recebe o endereço de um arquivo de entrada, contendo os modulos possiveis, as regras e o tamanho desejado da grid.
*/
void wfc(char* adress){
    double inicio, fim, delta;
    
    GET_TIME(inicio);
    
    __lowest_entropy=__num_modules;
    __num_cells_lowest_entropy = 0;



    __affected_cells = createQueue();
    __lowest_entropy_cells = (struct node**)malloc(sizeof(struct node*));
    __lowest_entropy_cells[0] = NULL;


    //Cria e preenche a grid
    Cell_2d** grid;
    //grid = setup(adress);
    //grid = setup_test(); //teste base
    printf("Lendo entrada\n");
    grid = setup(adress);
    //posicao randomica na lista de celulas de menor entropia
    int rand_cell=0;

    //o node retirado da lista de celulas de menor entropia
    struct node* next_node;

    //proxima posicao a ser collapsada
    Pos_2d next;
    next.lin = choose_rand(__size_lin-1);
    next.col = choose_rand(__size_col-1);

    //primeira inserção na lista
    insertLast_non_repete(__lowest_entropy_cells, next);
    __num_cells_lowest_entropy=1;



    

    //roda enquanto houverem celulas nao colapsadas
    while(!isEmpty(__lowest_entropy_cells)){
        //deletando a proxima celula a ser collapsada da lista
        delete(__lowest_entropy_cells, rand_cell);
        __num_cells_lowest_entropy--;


        //collapsando celula

        if(__print == 's'){
            printf("Colapsando (%d,%d)\n", next.lin+1, next.col+1);
        }
        
        collapse(grid, next);

        if(__print == 's'){
            printf("ncle: %d\n", __num_cells_lowest_entropy);
        }
        


        //se a lista de celulas de menor entropia esta vazia, faz uma varredura da grid por celulas nao collapsadas
        if(__num_cells_lowest_entropy == 0){
            __lowest_entropy = __num_modules;
            backtrack(grid);
        }


        //proxima celula a ser collapsada
        rand_cell = choose_rand(__num_cells_lowest_entropy-1);

       
        //node da proxima celula a ser collapsada
        next_node = find(__lowest_entropy_cells, rand_cell); 
        if(next_node==NULL){
            break;
        }


        //atualizando next
        next = next_node->data;


        print_grid(grid);
    }
    print_grid(grid);


    /*teste de erro
    grid[30][30].modules[0] = grid[15][15].modules[0];
    grid[29][29].modules[0] = grid[14][14].modules[0];
    grid[28][28].modules[0] = grid[13][13].modules[0];
    grid[27][27].modules[0] = grid[12][12].modules[0];
    grid[26][26].modules[0] = grid[11][11].modules[0];
    */

    //checa corretude
    is_correct(grid);


    //frees
    free(grid);
    free(__lowest_entropy_cells);
    free(__affected_cells);


    GET_TIME(fim);
    delta = fim - inicio;

    printf("Tempo: %lf\n", delta);

    printf("Fim!\n");

    
}




int main(int argc, char ** argv){
    __print = *argv[2];
    wfc(argv[1]);
    //setup(argv[1]);
}

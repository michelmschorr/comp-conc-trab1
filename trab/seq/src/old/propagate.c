/*
Propaga o efeito de um collapse sobre as celulas adjacentes
*/
void propagate(Cell_2d** grid, Pos_2d root_pos){
    
    Pos_2d target_pos;

    //adiciona a queue de celula com entropia alterada, que pode ser aumentada pelo constrain
    enQueue(__affected_cells, root_pos);

    while(__affected_cells->front!=NULL){
        target_pos = __affected_cells->front->key;
        deQueue(__affected_cells);


        //roda o constrain para cada celula adjacente
        if(target_pos.lin<__size_lin-1){
            constrain(&(grid[target_pos.lin][target_pos.col]), &(grid[target_pos.lin+1][target_pos.col]), 't');
        }
        if(target_pos.lin>0){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin-1][target_pos.col], 'b');
        }
        if(target_pos.col>0){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin][target_pos.col-1], 'l');
        }
        if(target_pos.col<__size_col-1){
            constrain(&grid[target_pos.lin][target_pos.col], &grid[target_pos.lin][target_pos.col+1], 'r');
        }

    }

}
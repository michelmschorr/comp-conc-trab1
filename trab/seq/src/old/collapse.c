

//Variaveis globais
extern int __num_cells_lowest_entropy;
extern int __lowest_entropy;   //menor entropia global

extern int __size_lin;
extern int __size_col;

extern int __num_modules;

extern struct Queue* __affected_cells;  
extern struct node** __lowest_entropy_cells;


/*
Inicia o collapso de uma celula e chama as funções que calculam todos os efeitos colaterais
*/
void collapse(Cell_2d** grid, Pos_2d pos){

    Cell_2d* cell = &grid[pos.lin][pos.col];
    

    //nao pode chamar collapse em celulas com entropia menor que 2
    if(cell->entropy <= 1) {  printf("Error: Tried to collapse cell with entropy of 1 or less\n"); exit(1);   }

    

    int ceiling = cell->entropy-1;
    int guess = choose_rand(ceiling);


    //torna nulos todas as referencias a modulos que não foram a escolhida e traz a nao nula para frente
    collapse_cell(cell, guess);


    propagate(grid, cell->pos);
}
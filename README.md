# Wave Function Collapse
## Trabalho de implementação
## Computação Concorrente - UFRJ - 2022.1
## Professora: Sylvana Rossetto
## Alunos:
### Michel Monteiro Schorr
### Yuri Vital Chalfun
### Pedro Henrique Figueiredo Von Zuben

<br><br><br>

## Descrição
 Implementação do algoritmo Wave Function Collapse para a geração de mapas 2d em terminal, usando a linguagem C e as bibliotecas pthreads e json-c.

 PS: A biblioteca json-c pode ter que ser instalada manualmente:

 Comando no Ubunto
 ```
 sudo apt-get install -y libjson-c-dev
 ```

 Nossa implementação le a altura da grid, comprimento da grid, os modulos e suas regras a partir de um arquivo json, como o setup.json fornecido.

 Endereço do arquivo json, numero de threads, e se deve printar a grid definidos no comando de execução

Descrição mais detalhada da implementação no relatório 


<br><br><br>

## Comandos

### Versão Sequencial
```
gcc ./trab/seq/src/wfc.c -o ./trab/seq/execs/wfc -ljson-c -lpthread

./trab/seq/execs/wfc /endereço_do_json.../setup.json n
```

PS: para printar a grid durante a execução, basta subsituir o "n" por "s"

PS: o nome do arquivo de entrada não precisa ser "setup"


<br><br><br>

### Versão Concorrente
```
gcc ./trab/conc/src/wfc.c -o ./trab/conc/execs/wfc -ljson-c -lpthread

./trab/conc/execs/wfc /endereço_do_json.../setup.json n 4
```

PS: para mudar o numero de threads, basta subsituir o "4" pelo numero desejado
#include "./print_grid.c"

//Variaveis globais
extern int __num_cells_lowest_entropy;
extern int __lowest_entropy;   //menor entropia global

extern int __size_lin;
extern int __size_col;

extern int __num_modules;

extern struct Queue* __affected_cells;  
extern struct node** __lowest_entropy_cells;

/*
Percorre a grid, adicionando as celulas com menor entropia na lista delas
*/
void backtrack(Cell_2d** grid){
    printf("backtrack\n");
    printf("ncle pre backtrack: %d\n", __num_cells_lowest_entropy);
    print_grid(grid);

    /*
    compara a entropia de cada celula que percorre com a menor_entropia(__lowest_entropy)
    caso menor, a substitui e limpa a lista de menores_entropias, 
    caso igual, adiciona a celula a lista
    */
    int entropy=__num_modules;
    for(int i =0; i<__size_lin; i++){
        for(int j =0; j<__size_col; j++){

            entropy = grid[i][j].entropy;

            if(entropy>1){
                if(entropy<__lowest_entropy){
                    clear(__lowest_entropy_cells);
                    __num_cells_lowest_entropy = 0;
                    __lowest_entropy = entropy;
                }
                if(entropy==__lowest_entropy){
                    insertLast(__lowest_entropy_cells, grid[i][j].pos);
                    __num_cells_lowest_entropy++;
                }
            }
        }
    }
    printf("ncle pos backtrack: %d\n", __num_cells_lowest_entropy);
}
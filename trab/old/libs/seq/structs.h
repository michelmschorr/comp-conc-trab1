#ifndef _STRUCTS_H 

typedef struct Color_rgba {
    char r;
    char g;
    char b;
    char* a;
} Color;




//modulo 2d
typedef struct Module_2d {
    //cor do modulo
    Color color;

    //vizinhos validos para esse modulo
    struct Module_2d** neigTop;
    int sizeNeigTop;

    struct Module_2d** neigBotton;
    int sizeNeigBotton;

    struct Module_2d** neigRight;
    int sizeNeigRight;

    struct Module_2d** neigLeft;
    int sizeNeigLeft;
} Module_2d;



//uma posição na grid
typedef struct Position_2d {
    int lin;
    int col;
} Pos_2d;



//celula 2d
typedef struct Cell_2d {
    //posicao da celula
    Pos_2d pos;

    //modulos validos
    Module_2d** modules;

    //entropia nessa posição
    int entropy;

} Cell_2d;

#endif
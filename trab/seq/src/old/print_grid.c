#include "../libs/linked_list.c"

//Variaveis globais
extern int __num_cells_lowest_entropy;
extern int __lowest_entropy;   //menor entropia global

extern int __size_lin;
extern int __size_col;

extern int __num_modules;

extern struct Queue* __affected_cells;  
extern struct node** __lowest_entropy_cells;

/*
Printa a grid para o terminal
*/
void print_grid(Cell_2d** grid){
    
    //borda superior
    for(int i =0; i< __size_col*2+2; i++) printf("-");
    printf("\n");
    

    for(int i =__size_lin-1; i>=0; i--){
        printf("|");//borda esquerda

        for(int j =0; j< __size_col; j++){
            grid[i][j].entropy != 1? printf("%2d", grid[i][j].entropy): printf("%s%s", grid[i][j].modules[0]->color.a, grid[i][j].modules[0]->color.a); 
        }

        printf("|");//borda direita
        printf("\n");
    }

    //borda inferior
    for(int i =0; i< __size_col*2+2; i++) printf("-");
    printf("\n");
    printf("\n");
    
}